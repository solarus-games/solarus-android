#!/usr/bin/env bash
# Build LuaJIT for all supported Android ABIs.
# script by gitlab.com/hhromic

set -Eeuo pipefail

NDK_ROOT=${NDK_ROOT:-/opt/android-sdk/ndk/latest}
LUAJIT_DIR=${LUAJIT_DIR:-$PWD/luajit}
PREBUILT_DIR=${PREBUILT_DIR:-$PWD/prebuilt}

if [[ ! -d $NDK_ROOT ]]; then
  printf "error: NDK_ROOT does not exist: %s\n" "$NDK_ROOT" >&2
  exit 1
fi

if [[ ! -d $LUAJIT_DIR ]]; then
  printf "error: LuaJIT directory does not exist: %s\n" "$LUAJIT_DIR" >&2
  exit 1
fi

if [[ -d $PREBUILT_DIR ]]; then
  printf "error: directory for prebuilt binaries already exists: %s\n" "$PREBUILT_DIR" >&2
  exit 1
fi
mkdir -p "$PREBUILT_DIR"

build() {
  local -r _abi=$1
  local -r _triplet=$2
  local -ra _extra_args=("${@:3}")
  local -r _prebuilt_dir=$PREBUILT_DIR/$_abi

  printf "\n> building LuaJIT for ABI '%s' using triplet '%s' (extra_args=%s)\n\n" \
    "$_abi" "$_triplet" "${_extra_args[*]}" >&2

  export PATH="$NDK_ROOT/toolchains/llvm/prebuilt/linux-x86_64/bin:$PATH"

  export HOST_CC="gcc ${_extra_args[*]}"
  export CC="clang"
  export CROSS="${_triplet}-"
  export STATIC_CC="${_triplet}-clang"
  export DYNAMIC_CC="${_triplet}-clang -fPIC"
  export TARGET_AR="llvm-ar rcus"
  export TARGET_SYS="Linux"
  export TARGET_LD="${_triplet}-clang"
  export TARGET_LDFLAGS="-fuse-ld=lld"
  export TARGET_STRIP="llvm-strip"
  export TARGET_SONAME="libluajit.so"
  export GIT_RELVER="cp ../.relver luajit_relver.txt"
  export INSTALL_ANAME="libluajit.a"
  export INSTALL_SONAME="libluajit.so"
  export INSTALL_INC=$_prebuilt_dir/include
  export INSTALL_LIB=$_prebuilt_dir/lib
  export INSTALL_BIN=$_prebuilt_dir/.dispose
  export INSTALL_SHARE=$_prebuilt_dir/.dispose
  export INSTALL_CMODD=$_prebuilt_dir/.dispose
  export INSTALL_PKGCONFIG=$_prebuilt_dir/.dispose
  export INSTALL_SHORT1=$_prebuilt_dir/.dispose/short1
  export INSTALL_SHORT2=$_prebuilt_dir/.dispose/short2

  cd "$LUAJIT_DIR"
  make -e clean amalg install

  rm -rf "$_prebuilt_dir/.dispose"
}

build arm64-v8a aarch64-linux-android21
build armeabi-v7a armv7a-linux-androideabi21 '-m32'
build x86 i686-linux-android21 '-m32'
build x86_64 x86_64-linux-android21
