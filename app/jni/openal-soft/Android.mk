LOCAL_PATH := $(call my-dir)

# search for the openal built on cmake side

include $(CLEAR_VARS)

ifeq ($(NDK_DEBUG),1)
	SOL_LIB_CONFIG=Debug
else
	SOL_LIB_CONFIG=RelWithDebInfo
endif

LOCAL_MODULE := openal
LOCAL_SRC_FILES := $(wildcard $(LOCAL_PATH)/../../../cmake_build/build/intermediates/cxx/$(SOL_LIB_CONFIG)/*/obj/$(TARGET_ARCH_ABI)/libopenal.so)
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/../../../cmake_build/src/main/cpp/openal-soft/include

include $(PREBUILT_SHARED_LIBRARY)
