package org.solarus_games.solarus.common

import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.runtime.Composable

abstract class ComposeActivity : AppCompatActivity() {
    @Composable
    abstract fun GetContent()

    abstract fun setup()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setup()
        setContent {
            SolarusThemeSurface {
                GetContent()
            }
        }
    }
}