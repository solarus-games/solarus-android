package org.solarus_games.solarus

import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.ActivityInfo
import android.content.res.Configuration
import android.graphics.Bitmap
import android.os.Bundle
import android.os.Handler
import android.view.KeyEvent
import android.view.PixelCopy
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import androidx.activity.OnBackPressedCallback
import androidx.window.layout.WindowMetrics
import androidx.window.layout.WindowMetricsCalculator
import dagger.hilt.android.AndroidEntryPoint
import org.libsdl.app.SDLActivity
import org.solarus_games.solarus.joypad.ButtonMappingManager
import org.solarus_games.solarus.joypad.ButtonMappingManager.InputLayout
import org.solarus_games.solarus.preferences.PreferencesManager
import org.solarus_games.solarus.quest.QuestManager
import org.solarus_games.solarus.quest.QuestManagerListener
import org.solarus_games.solarus.utils.setLayoutPosition
import javax.inject.Inject


@AndroidEntryPoint
class EngineActivity : SDLActivity(), QuestManagerListener {

    private var inputLayout: InputLayout? = null

    private lateinit var mButtonLayout: RelativeLayout

    @Inject
    lateinit var preferencesManager: PreferencesManager

    @Inject
    lateinit var questManager: QuestManager

    @Inject
    lateinit var buttonMappingManager: ButtonMappingManager

    override fun getLibraries(): Array<String> {
        return arrayOf(
            "SDL2",
             "SDL2_image",
            // "SDL2_mixer",
            // "SDL2_net",
             "SDL2_ttf",
            "main"
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        questManager.setCurrentQuest(intent.extras?.getString(ARG_PATH)?.let { questManager.fromPath(it) })
        questManager.addListener(this)

        mButtonLayout = RelativeLayout(this)

        onBackPressedDispatcher.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                grabScreenCapture()
            }
        })

        // Screen orientation
        if (preferencesManager.isForcedLandscape()) {
            this.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
        }
        mLayout.addView(mButtonLayout)
    }

    override fun onDestroy() {
        questManager.removeListener(this)
        super.onDestroy()
    }

    private fun bringMainActivityToFront() {
        val i = Intent(applicationContext, MainActivity::class.java)
        i.flags = Intent.FLAG_ACTIVITY_REORDER_TO_FRONT
        startActivity(i)
    }

    @SuppressLint("GestureBackNavigation")
    override fun dispatchKeyEvent(event: KeyEvent): Boolean {
        if (android.os.Build.VERSION.SDK_INT <= 34) {
            val keyCode = event.keyCode
            // Ignore certain special keys so they're handled by Android
            if (keyCode == KeyEvent.KEYCODE_BACK) {
                grabScreenCapture()
                return true //Let the back button go up
            }
        }
        return super.dispatchKeyEvent(event)
    }

    fun grabScreenCapture() {
        val sv: View = mSurface
        val width = sv.width
        val height = sv.height
        val bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
        PixelCopy.request(mSurface, bitmap, {
            questManager.setScreenCapture(bitmap)
            bringMainActivityToFront()
        }, Handler(mainLooper))
    }

    override fun exit() {
        finish()
    }

    override fun getArguments(): Array<String> {
        val args = ArrayList<String>()
        args.add("-fullscreen=yes")
        if (!preferencesManager.isAudioEnabled()) {
            args.add("-no-audio")
        }
        args.add(questManager.currentQuest.value?.path.orEmpty())
        return args.toTypedArray()
    }

    public override fun onResume() {
        super.onResume()
        spawnGamepad()
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)

        // Replace buttons on screen
        if (preferencesManager.isGamepadEnabled()) {
            updateButtonsPosition()
        }
    }

    private fun spawnGamepad() {
        mButtonLayout.removeAllViews()
        if (preferencesManager.isGamepadEnabled()) {
            // Choose the proper InputLayout //TODO don't use only default
            inputLayout = buttonMappingManager.getLayoutById(buttonMappingManager.defaultLayoutId)

            // Add buttons
            addButtons()
        }
    }

    /**
     * Draws all buttons.
     */
    private fun addButtons() {

        // Adding the buttons
        inputLayout?.let {
            if (it.buttonList.isNotEmpty()) {
                for (b in it.buttonList) {
                    // We add it, if it's not the case already
                    if (b.parent !== mButtonLayout) {
                        if (b.parent != null) {
                            (b.parent as ViewGroup).removeAllViews()
                        }
                        mButtonLayout.addView(b)
                    }
                }
                updateButtonsPosition()
            }
        }
    }

    private fun updateButtonsPosition() {
        var params: RelativeLayout.LayoutParams
        val windowMetrics: WindowMetrics = WindowMetricsCalculator.getOrCreate().computeCurrentWindowMetrics(this)
        val screenWidth = windowMetrics.bounds.width()
        val screenHeight = windowMetrics.bounds.height()
        for (b in inputLayout!!.buttonList) {
            setLayoutPosition(this, b, b.posX, b.posY)

            // We have to adjust the position in portrait configuration
            if (resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT) {
                params = b.layoutParams as RelativeLayout.LayoutParams
                // vertical : use approximately the second part of the screen
                params.topMargin += (screenHeight / 6)
                // horizontal : use a little gap to avoid button to be out of
                // the screen for button to the right
                if (b.posX > 0.5) {
                    params.leftMargin -= screenWidth / 8
                }
                b.layoutParams = params
            }
        }
    }

    override fun setOrientationBis(w: Int, h: Int, resizable: Boolean, hint: String?) {
        if (preferencesManager.isForcedLandscape()) return
        super.setOrientationBis(w, h, resizable, hint)
    }

    companion object {
        const val ARG_PATH = "path"
    }
}