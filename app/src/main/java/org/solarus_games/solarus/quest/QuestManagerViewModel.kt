package org.solarus_games.solarus.quest

import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import org.solarus_games.solarus.remote.models.Game
import javax.inject.Inject

@HiltViewModel
class QuestManagerViewModel @Inject constructor(
    private val questManager: QuestManager
) : ViewModel() {

    fun deleteAllRemoteQuests() = questManager.deleteAllRemoteQuests()

    fun currentQuest() = questManager.currentQuest

    fun getQuestForGame(game: Game) = questManager.getQuestForGameId(game.id)

}
