LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE    := libvorbis
OGG_DIR         := external/libogg
LOCAL_C_INCLUDES += $(LOCAL_PATH)/include $(LOCAL_PATH)/lib $(OGG_DIR)/include
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_C_INCLUDES)

LOCAL_SRC_FILES := $(wildcard $(LOCAL_PATH)/lib/*.c)
LOCAL_SRC_FILES := $(filter-out $(LOCAL_PATH)/lib/psytune.c, $(LOCAL_SRC_FILES))

LOCAL_CFLAGS := -Wall -Werror \
	-Wno-unused-function \
	-Wno-unused-parameter \
	-Wno-unused-variable \
	-Wno-unused-but-set-variable \
	-Wno-excess-initializers \
	-Wno-missing-braces \
	-Wno-int-conversion \
	-Wno-braced-scalar-init \
	-Wno-incompatible-pointer-types \
	-Wno-implicit-function-declaration \
	-Wno-pointer-sign

LOCAL_STATIC_LIBRARIES := libogg

LOCAL_SDK_VERSION := 14

include $(BUILD_STATIC_LIBRARY)
