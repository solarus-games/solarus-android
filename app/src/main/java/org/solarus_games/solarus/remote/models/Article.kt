package org.solarus_games.solarus.remote.models

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Article(
    val date: String,
    val excerpt: String,
    val permalink: String,
    val thumbnail: String,
    val title: String
)