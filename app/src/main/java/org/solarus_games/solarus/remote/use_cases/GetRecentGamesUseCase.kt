package org.solarus_games.solarus.remote.use_cases

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.combineTransform
import org.solarus_games.solarus.common.Logger
import org.solarus_games.solarus.quest.Quest
import org.solarus_games.solarus.quest.QuestManager
import org.solarus_games.solarus.recent.RecentlyPlayedDao
import javax.inject.Inject

class GetRecentGamesUseCase @Inject constructor(
    private val rpDao: RecentlyPlayedDao,
    private val questManager: QuestManager,
    private val logger: Logger
) {

    operator fun invoke(): Flow<List<Quest>> =
        combineTransform(questManager.quests, rpDao.getRecentGames()) { existingQuests, recentQuests ->
            emit(recentQuests.mapNotNull { recent -> existingQuests.find { it.path == recent.path } })
        }.catch { e ->
            logger.e("GetGamesUseCase", "Error", e)
            emit(emptyList())
        }
}