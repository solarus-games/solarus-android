package org.solarus_games.solarus.recent

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "recently_played")
data class RecentlyPlayed(
    @PrimaryKey val path: String,
    @ColumnInfo(name = "last_played") val lastPlayed: Long = System.currentTimeMillis(),
)