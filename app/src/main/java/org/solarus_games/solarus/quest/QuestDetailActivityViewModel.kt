package org.solarus_games.solarus.quest

import android.content.Context
import android.widget.Toast
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import org.solarus_games.solarus.common.Logger
import org.solarus_games.solarus.common.Resource
import org.solarus_games.solarus.preferences.PreferencesManager
import org.solarus_games.solarus.remote.models.Game
import org.solarus_games.solarus.remote.use_cases.DownloadGameUseCase
import org.solarus_games.solarus.remote.use_cases.GetGamesUseCase
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class QuestDetailActivityViewModel @Inject constructor(
    val logger: Logger,
    val preferencesManager: PreferencesManager,
    val questManager: QuestManager,
    private val getGamesUseCase: GetGamesUseCase,
    private val downloadGameUseCase: DownloadGameUseCase
) : ViewModel() {

    var quest = MutableStateFlow<Quest?>(null)
        private set

    var game = MutableStateFlow<Game?>(null)
        private set

    val hasUpdate = combine(
        quest,
        game,
    ) { q, g ->
        (q != null && g != null && q.version != g.version)
    }

    fun updateQuestState(q: Quest) {
        viewModelScope.launch {
            quest.emit(q)
        }
    }

    fun collectRemoteDataForId(remoteId: String?) {
        if (remoteId != null) {
            getGamesUseCase().onEach { result ->
                if (result.data?.isNotEmpty() == true) {
                    result.data.find { it.id == remoteId }?.let {
                        game.emit(it)
                    }
                }
            }.launchIn(viewModelScope)
        } else {
            viewModelScope.launch {
                game.emit(null)
            }
        }
    }

    fun launchQuest(context: Context) {
        quest.value?.let {
            questManager.launchQuest(context, it)
        }
    }

    fun deleteQuest() {
        quest.value?.let {
            questManager.deleteQuestByPath(it.path)
        }
    }

    fun downloadGame(context: Context) {
        game.value?.let {
            downloadGameUseCase(context, it).onEach { result ->
                when (result) {
                    is Resource.Loading -> {
                        // do nothing dialog handled by use case
                    }

                    is Resource.Success -> {
                        game.emit(it)
                        quest.emit(questManager.getQuestForGameId(it.id))
                    }

                    is Resource.Error -> {
                        Toast.makeText(
                            context,
                            "Error Downloading Game",
                            Toast.LENGTH_LONG
                        ).show()
                    }
                }
            }.launchIn(viewModelScope)
        }
    }
}