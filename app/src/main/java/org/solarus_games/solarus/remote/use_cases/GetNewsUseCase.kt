package org.solarus_games.solarus.remote.use_cases

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow
import org.solarus_games.solarus.common.Logger
import org.solarus_games.solarus.remote.RemoteRepository
import org.solarus_games.solarus.remote.models.Article
import javax.inject.Inject

class GetNewsUseCase @Inject constructor(
    private val remoteRepository: RemoteRepository,
    private val logger: Logger
) {

    operator fun invoke(useCache: Boolean = true): Flow<List<Article>> = flow {
        val articles = remoteRepository.getNews(useCache)
        emit(articles)
    }.catch { e ->
        logger.e("GetNewsUseCase", "Error", e)
        emit(emptyList())
    }
}