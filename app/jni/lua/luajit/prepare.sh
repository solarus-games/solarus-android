#!/usr/bin/env bash
# Prepare a source directory for building LuaJIT.
# script by gitlab.com/hhromic

set -Eeuo pipefail

REPOSITORY=${REPOSITORY:-https://github.com/luajit/luajit}
BRANCH=${BRANCH:-v2.1}
LUAJIT_DIR=${LUAJIT_DIR:-$PWD/luajit}

if [[ -d $LUAJIT_DIR ]]; then
  printf "error: directory for LuaJIT already exists: %s\n" "$LUAJIT_DIR" >&2
  exit 1
fi
mkdir -p "$LUAJIT_DIR"

TMPDIR=$(mktemp -d)
trap 'rm -rf -- "$TMPDIR"' EXIT

git clone "$REPOSITORY" --branch "$BRANCH" --depth 1 "$TMPDIR"
git -C "$TMPDIR" archive --format tar HEAD | tar xf - -C "$LUAJIT_DIR"
