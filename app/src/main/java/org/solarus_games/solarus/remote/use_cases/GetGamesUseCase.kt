package org.solarus_games.solarus.remote.use_cases

import android.app.Application
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow
import org.solarus_games.solarus.R
import org.solarus_games.solarus.common.Logger
import org.solarus_games.solarus.common.Resource
import org.solarus_games.solarus.remote.RemoteRepository
import org.solarus_games.solarus.remote.models.Game
import javax.inject.Inject

class GetGamesUseCase @Inject constructor(
    private val application: Application,
    private val remoteRepository: RemoteRepository,
    private val logger: Logger
) {

    operator fun invoke(useCache: Boolean = true): Flow<Resource<List<Game>>> = flow {
        emit(Resource.Loading())
        val games = remoteRepository.getGames(useCache).filter { game -> game.download.isNotEmpty() }
        emit(Resource.Success(games))
    }.catch { e ->
        logger.e("GetGamesUseCase", "Error", e)
        emit(Resource.Error(message = application.getString(R.string.games_error)))
    }
}