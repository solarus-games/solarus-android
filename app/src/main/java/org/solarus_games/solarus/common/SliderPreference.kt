package org.solarus_games.solarus.common

import androidx.annotation.StringRes
import androidx.compose.foundation.layout.Column
import androidx.compose.material3.Slider
import androidx.compose.material3.SliderDefaults.colors
import androidx.compose.runtime.Composable
import androidx.compose.ui.res.stringResource
import org.solarus_games.solarus.theme.PrimaryLight
import org.solarus_games.solarus.theme.Secondary
import kotlin.math.roundToInt

@Composable
fun SliderPreference(
    @StringRes title: Int,
    value: Int,
    onValueChange: (value: Int) -> Unit
) {
    Column {
        PreferenceHeader(stringResource(id = title))
        Slider(
            value = value.toFloat(),
            onValueChange = {
                onValueChange(it.roundToInt())
            },
            valueRange = 0f..255f,
            colors = colors(thumbColor = Secondary, activeTrackColor = Secondary, inactiveTrackColor = PrimaryLight)
        )
    }
}