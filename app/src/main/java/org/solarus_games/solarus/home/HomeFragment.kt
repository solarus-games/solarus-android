package org.solarus_games.solarus.home

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.ArrowBack
import androidx.compose.material.pullrefresh.PullRefreshIndicator
import androidx.compose.material.pullrefresh.pullRefresh
import androidx.compose.material.pullrefresh.rememberPullRefreshState
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.CenterAlignedTopAppBar
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.geometry.CornerRadius
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.graphics.PathEffect
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.core.os.bundleOf
import androidx.fragment.app.setFragmentResult
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import org.solarus_games.solarus.NowPlayingFragment
import org.solarus_games.solarus.R
import org.solarus_games.solarus.common.Center
import org.solarus_games.solarus.common.Constants.COLUMN_PADDING
import org.solarus_games.solarus.common.Constants.GAMES_INDEX
import org.solarus_games.solarus.common.Constants.INDEX_KEY
import org.solarus_games.solarus.common.Constants.LARGE_CARD_CORNERS
import org.solarus_games.solarus.common.Constants.NAVIGATION_KEY
import org.solarus_games.solarus.common.Constants.SHOP_INDEX
import org.solarus_games.solarus.quest.QuestIconView
import org.solarus_games.solarus.remote.models.HomeScreen
import org.solarus_games.solarus.theme.Primary
import org.solarus_games.solarus.theme.PrimaryLight
import org.solarus_games.solarus.theme.Secondary
import org.solarus_games.solarus.theme.SecondaryLight
import org.solarus_games.solarus.theme.SecondaryText
import org.solarus_games.solarus.theme.SolarusTypography
import javax.inject.Inject

@AndroidEntryPoint
class HomeFragment : NowPlayingFragment() {

    @Inject
    lateinit var viewModel: HomeViewModel

    @OptIn(ExperimentalMaterial3Api::class)
    @Composable
    override fun GetHeader() {
        CenterAlignedTopAppBar(
            colors = TopAppBarDefaults.topAppBarColors(containerColor = Primary),
            title = {
                Image(
                    painter = painterResource(id = R.drawable.solarus_logo_white),
                    contentDescription = "Icon",
                    Modifier
                        .size(300.dp, 50.dp)
                        .testTag("Icon")
                )
            }
        )
    }

    @OptIn(ExperimentalMaterialApi::class)
    @Composable
    override fun GetContent() {
        val refreshScope = rememberCoroutineScope()
        var refreshing by remember { mutableStateOf(false) }

        fun refresh() = refreshScope.launch {
            refreshing = true
            viewModel.getHomeScreen(false)
            refreshing = false
        }

        val state = rememberPullRefreshState(refreshing, ::refresh)
        Box(Modifier.pullRefresh(state)) {
            MainContent()
            PullRefreshIndicator(refreshing, state, Modifier.align(Alignment.TopCenter))
        }
    }

    @Composable
    fun MainContent() {
        val context = LocalContext.current
        val state by viewModel.state.collectAsState(HomeScreen(loading = true))
        with(state) {
            when {
                loading -> Center(Modifier.fillMaxSize()) { CircularProgressIndicator() }
                else -> LazyColumn(
                    modifier = Modifier
                        .fillMaxSize()
                        .padding(horizontal = COLUMN_PADDING.dp),
                    verticalArrangement = Arrangement.spacedBy(20.dp),
                    horizontalAlignment = Alignment.CenterHorizontally,
                    contentPadding = PaddingValues(vertical = 15.dp)
                ) {
                    item {
                        Spacer(modifier = Modifier.height(5.dp))
                    }
                    item {
                        Text(
                            stringResource(id = R.string.recent_games),
                            color = SecondaryLight,
                            style = SolarusTypography.headlineLarge,
                            modifier = Modifier.fillMaxWidth(),
                            textAlign = TextAlign.Start
                        )
                    }
                    if (recents.isNotEmpty()) {
                        item {
                            with(recents.first()) {
                                QuestIconView(
                                    modifier = Modifier
                                        .clip(RoundedCornerShape(LARGE_CARD_CORNERS.dp))
                                        .fillMaxWidth()
                                        .height(200.dp),
                                    quest = this,
                                    true
                                ) {
                                    viewModel.onQuestSelected(context, this)
                                }
                            }
                        }
                        if (recents.size > 2) {
                            item {
                                Row(Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.spacedBy(10.dp)) {
                                    with(recents[1]) {
                                        QuestIconView(
                                            modifier = Modifier
                                                .clip(RoundedCornerShape(LARGE_CARD_CORNERS.dp))
                                                .weight(1f)
                                                .height(90.dp),
                                            quest = this
                                        ) {
                                            viewModel.onQuestSelected(context, this)
                                        }
                                    }
                                    with(recents[2]) {
                                        QuestIconView(
                                            modifier = Modifier
                                                .clip(RoundedCornerShape(LARGE_CARD_CORNERS.dp))
                                                .weight(1f)
                                                .height(90.dp),
                                            quest = this
                                        ) {
                                            viewModel.onQuestSelected(context, this)
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        item {
                            val stroke = Stroke(
                                width = 8.dp.value,
                                pathEffect = PathEffect.dashPathEffect(floatArrayOf(25f, 20f), 0f)
                            )
                            Box(
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .height(200.dp)
                                    .drawBehind {
                                        drawRoundRect(
                                            color = PrimaryLight,
                                            style = stroke,
                                            cornerRadius = CornerRadius(20.dp.value, 20.dp.value)
                                        )
                                    },
                                contentAlignment = Alignment.Center
                            ) {
                                Icon(
                                    painterResource(id = R.drawable.gamepad),
                                    "No Recents Icon",
                                    tint = PrimaryLight,
                                    modifier = Modifier.size(50.dp)
                                )
                            }
                        }

                        item {
                            Text(
                                stringResource(id = R.string.no_recents_title),
                                style = SolarusTypography.bodyLarge.copy(fontWeight = FontWeight.Light),
                                maxLines = 2,
                                color = SecondaryText,
                                textAlign = TextAlign.Center
                            )
                        }

                        item {
                            Button(onClick = {
                                setFragmentResult(
                                    NAVIGATION_KEY,
                                    bundleOf(INDEX_KEY to (if (viewModel.hasQuests()) GAMES_INDEX else SHOP_INDEX))
                                )
                            }, colors = ButtonDefaults.buttonColors(containerColor = Secondary)) {
                                Image(
                                    imageVector = Icons.AutoMirrored.Default.ArrowBack, contentDescription = "Resume",
                                    modifier = Modifier
                                        .size(24.dp)
                                        .rotate(180F),
                                    colorFilter = ColorFilter.tint(Color.White)
                                )

                                Text(stringResource(id = R.string.play_quests), Modifier.padding(start = 5.dp))
                            }
                        }
                    }
                    item {
                        Spacer(modifier = Modifier.height(5.dp))
                    }
                    if (news.isNotEmpty()) {
                        item {
                            Text(
                                stringResource(id = R.string.latest_news),
                                color = SecondaryLight,
                                style = SolarusTypography.headlineLarge,
                                modifier = Modifier.fillMaxWidth(),
                                textAlign = TextAlign.Start
                            )
                        }

                        items(news) { article ->
                            ArticleView(
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .height(200.dp), article = article
                            )
                        }
                    }
                }
            }
        }
    }
}
