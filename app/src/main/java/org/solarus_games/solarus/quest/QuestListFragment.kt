package org.solarus_games.solarus.quest

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.ArrowBack
import androidx.compose.material.icons.outlined.Info
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.core.os.bundleOf
import androidx.fragment.app.setFragmentResult
import dagger.hilt.android.AndroidEntryPoint
import org.solarus_games.solarus.BaseQuestListFragment
import org.solarus_games.solarus.R
import org.solarus_games.solarus.common.Center
import org.solarus_games.solarus.common.Constants
import org.solarus_games.solarus.common.Constants.COLUMN_PADDING
import org.solarus_games.solarus.common.Constants.LARGE_CARD_CORNERS
import org.solarus_games.solarus.preferences.FolderPreferenceFragment
import org.solarus_games.solarus.theme.PrimaryLight
import org.solarus_games.solarus.theme.Secondary
import org.solarus_games.solarus.theme.SecondaryLight
import org.solarus_games.solarus.theme.SecondaryText
import org.solarus_games.solarus.theme.SolarusTypography
import javax.inject.Inject


@AndroidEntryPoint
class QuestListFragment : BaseQuestListFragment<Quest>() {

    @Inject
    lateinit var viewModel: QuestListFragmentViewModel

    override val getHeaderStringRes = R.string.games

    override fun getPreferencesManager() = viewModel.preferencesManager
    override fun handleRefresh() = viewModel.refreshQuests()

    override fun shouldFilter(search: String, item: Quest) = item.title.contains(search, true)

    @Composable
    override fun GetContent() {
        val questState by viewModel.state.collectAsState(initial = QuestsState(isLoading = true))
        with(questState) {
            when {
                isLoading -> Center(modifier = Modifier.fillMaxSize()) {
                    CircularProgressIndicator()
                }

                else -> QuestListView(quests)
            }
        }
    }

    @Composable
    override fun GetItemView(item: Quest) {
        val context = LocalContext.current
        val useGrid by viewModel.preferencesManager.useGridViewFlow().collectAsState(initial = false)
        if (useGrid) {
            QuestIconView(
                modifier = Modifier
                    .padding(horizontal = COLUMN_PADDING.dp)
                    .clip(RoundedCornerShape(LARGE_CARD_CORNERS.dp))
                    .fillMaxWidth()
                    .height(200.dp), quest = item, true
            ) {
                viewModel.onQuestSelected(context, item)
            }
        } else {
            QuestColumnView(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 10.dp), quest = item
            ) {
                viewModel.onQuestSelected(context, item)
            }
        }
    }

    @Composable
    override fun GetErrorView() {
        Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 30.dp),
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.spacedBy(15.dp),
            ) {
                Icon(
                    imageVector = Icons.Outlined.Info,
                    contentDescription = "Info Button",
                    modifier = Modifier.size(48.dp),
                    tint = SecondaryText
                )
                Text(
                    stringResource(R.string.no_quests_found_title),
                    style = SolarusTypography.headlineSmall,
                    color = SecondaryLight,
                    textAlign = TextAlign.Center
                )
                Text(
                    stringResource(R.string.no_quests_found),
                    style = SolarusTypography.bodyLarge,
                    color = SecondaryText,
                    textAlign = TextAlign.Center
                )
                Text(
                    stringResource(R.string.download_or_choose),
                    style = SolarusTypography.bodyLarge,
                    color = SecondaryText,
                    textAlign = TextAlign.Center
                )
                Spacer(modifier = Modifier.height(10.dp))
                Button(onClick = {
                    setFragmentResult(
                        Constants.NAVIGATION_KEY,
                        bundleOf(Constants.INDEX_KEY to Constants.SHOP_INDEX)
                    )
                }, colors = ButtonDefaults.buttonColors(containerColor = Secondary)) {
                    Image(
                        imageVector = Icons.AutoMirrored.Default.ArrowBack, contentDescription = "Browse Store",
                        modifier = Modifier
                            .size(24.dp)
                            .rotate(180F),
                        colorFilter = ColorFilter.tint(Color.White)
                    )

                    Text(stringResource(id = R.string.browse_store), Modifier.padding(start = 5.dp))
                }

                Button(onClick = {
                    FolderPreferenceFragment.newInstance().show(
                        requireActivity().supportFragmentManager,
                        FolderPreferenceFragment.TAG
                    )
                }, colors = ButtonDefaults.buttonColors(containerColor = PrimaryLight)) {
                    Image(
                        painter = painterResource(id = R.drawable.settings), contentDescription = "Change Directory",
                        modifier = Modifier
                            .size(24.dp),
                        colorFilter = ColorFilter.tint(Color.White)
                    )

                    Text(stringResource(id = R.string.change_directory), Modifier.padding(start = 5.dp))
                }
            }
        }
    }
}