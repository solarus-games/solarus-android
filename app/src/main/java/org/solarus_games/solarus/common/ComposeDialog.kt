package org.solarus_games.solarus.common

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.annotation.StringRes
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color.Companion.White
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.fragment.app.DialogFragment
import org.solarus_games.solarus.R
import org.solarus_games.solarus.theme.Secondary
import org.solarus_games.solarus.theme.SolarusTheme

abstract class ComposeDialog : DialogFragment() {

    val loading = mutableStateOf(false)
    val error = mutableStateOf(false)

    fun setLoading(isLoading: Boolean) {
        loading.value = isLoading
    }

    fun showError(showError: Boolean) {
        error.value = showError
    }

    @Composable
    abstract fun GetContent()

    @StringRes
    open fun errorMessage() = R.string.error_dialog

    @Composable
    fun GetError() {
        Column(
            verticalArrangement = Arrangement.spacedBy(20.dp),
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier
                .fillMaxWidth()
                .wrapContentHeight()
                .padding(20.dp)
        ) {
            Text(
                stringResource(errorMessage()),
                textAlign = TextAlign.Center,
                fontFamily = FontFamily.Serif,
                maxLines = 3,
                overflow = TextOverflow.Ellipsis,
                fontSize = 24.sp,
                fontWeight = FontWeight.Bold,
                modifier = Modifier.fillMaxWidth()
            )
            Button(
                onClick = {
                    error.value = false
                },
                shape = RoundedCornerShape(20.dp),
                colors = ButtonDefaults.buttonColors(containerColor = Secondary),
            ) {
                Text(
                    stringResource(R.string.ok),
                    color = White,
                    textAlign = TextAlign.Center,
                    fontFamily = FontFamily.Serif,
                    maxLines = 1,
                    overflow = TextOverflow.Ellipsis,
                    fontSize = 16.sp,
                    fontWeight = FontWeight.Bold,
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(10.dp)
                )
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = ComposeView(requireContext()).apply {
        setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
        setContent {
            SolarusTheme {
                Surface(
                    modifier = Modifier
                        .fillMaxWidth()
                        .wrapContentHeight(),
                    shape = RoundedCornerShape(15.dp)
                ) {
                    Box(modifier = Modifier.padding(10.dp)) {
                        when {
                            loading.value -> CircularProgressIndicator(
                                modifier = Modifier
                                    .fillMaxSize()
                                    .padding(100.dp),
                                strokeWidth = 10.dp,
                                color = Secondary
                            )

                            error.value -> GetError()
                            else -> GetContent()
                        }
                    }
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        dialog!!.window?.let { window ->
            val params: WindowManager.LayoutParams = window.attributes
            params.width = WindowManager.LayoutParams.MATCH_PARENT
            params.height = WindowManager.LayoutParams.WRAP_CONTENT
            window.attributes = params
            window.setBackgroundDrawableResource(android.R.color.transparent);
        }
    }
}