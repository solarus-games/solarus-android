package org.solarus_games.solarus

import android.content.Intent
import android.graphics.Bitmap
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.CenterAlignedTopAppBar
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.ExtendedFloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.core.content.pm.ShortcutInfoCompat
import androidx.core.content.pm.ShortcutManagerCompat
import androidx.core.graphics.drawable.IconCompat
import kotlinx.coroutines.flow.Flow
import org.solarus_games.solarus.common.ComposeActivity
import org.solarus_games.solarus.common.QuestProperties
import org.solarus_games.solarus.quest.NowPlayingView
import org.solarus_games.solarus.quest.Quest
import org.solarus_games.solarus.quest.QuestManager
import org.solarus_games.solarus.theme.Primary
import org.solarus_games.solarus.theme.PrimaryDarkish
import org.solarus_games.solarus.theme.PrimaryLight
import org.solarus_games.solarus.theme.PrimaryMedium
import org.solarus_games.solarus.theme.Secondary
import org.solarus_games.solarus.theme.SecondaryLight
import org.solarus_games.solarus.theme.SecondaryTextDark
import org.solarus_games.solarus.theme.SecondaryTextLight
import org.solarus_games.solarus.theme.SolarusTypography
import org.solarus_games.solarus.theme.ubuntu_regular
import java.time.LocalDate
import java.time.format.DateTimeFormatter

abstract class BaseQuestDetailActivity : ComposeActivity() {

    abstract fun getQuestManager(): QuestManager

    abstract fun onPlayClick()
    abstract fun onUpdateClick()

    abstract fun getQuestTitle(): Flow<String>
    abstract fun getQuestDescription(): Flow<String>

    abstract fun hasUpdate(): Flow<Boolean>

    @Composable
    abstract fun GetHeaderActions()

    @Composable
    abstract fun GetThumbnailView()

    @Composable
    abstract fun GetPropertyView(modifier: Modifier, property: QuestProperties)

    abstract fun getProperties(): Flow<List<QuestProperties>>


    @Composable
    override fun GetContent() {
        val currentQuest by getQuestManager().currentQuest.collectAsState(null)
        Box(modifier = Modifier.fillMaxSize()) {
            GameContent()
            ExtendedFloatingActionButton(
                modifier = Modifier
                    .align(Alignment.BottomEnd)
                    .padding(20.dp),
                contentColor = Color.White,
                containerColor = Secondary,
                shape = RoundedCornerShape(15.dp),
                onClick = {
                    onPlayClick()
                },
                icon = {
                    Icon(
                        painter = painterResource(id = R.drawable.play),
                        contentDescription = "Play button"
                    )
                },
                text = { Text(text = stringResource(id = R.string.play_quest)) },
            )
            if (currentQuest != null) {
                NowPlayingView(
                    modifier = Modifier
                        .align(Alignment.BottomCenter), manager = getQuestManager()
                )
            }
        }
    }

    @OptIn(ExperimentalFoundationApi::class)
    @Composable
    fun GameContent() {
        val description by getQuestDescription().collectAsState(initial = "")
        val properties by getProperties().collectAsState(initial = emptyList())
        val hasUpdate by hasUpdate().collectAsState(initial = false)
        LazyColumn(
            modifier = Modifier.fillMaxSize(),
            horizontalAlignment = Alignment.CenterHorizontally,
            contentPadding = PaddingValues(bottom = 30.dp)
        ) {
            stickyHeader { Header() }

            item {
                GetThumbnailView()
            }

            if (hasUpdate) {
                item {
                    Column(horizontalAlignment = Alignment.CenterHorizontally) {
                        Text(
                            text = stringResource(id = R.string.update_available),
                            style = textStyle,
                            modifier = Modifier
                                .padding(horizontal = 30.dp)
                                .padding(vertical = 10.dp)
                        )

                        Button(onClick = {
                            onUpdateClick()
                        }, colors = ButtonDefaults.buttonColors(containerColor = PrimaryLight)) {
                            Image(
                                painter = painterResource(id = R.drawable.refresh), contentDescription = "Update",
                                modifier = Modifier
                                    .size(24.dp),
                                colorFilter = ColorFilter.tint(Color.White)
                            )

                            Text(stringResource(id = R.string.update).uppercase(), Modifier.padding(start = 5.dp))
                        }
                    }
                }
            }

            item {
                Text(
                    description,
                    style = textStyle,
                    modifier = Modifier
                        .padding(horizontal = 30.dp)
                        .padding(vertical = 20.dp)
                )
            }

            properties.forEachIndexed { i, prop ->
                val modifier = when {
                    i == 0 -> Modifier
                        .padding(horizontal = 30.dp)
                        .clip(RoundedCornerShape(topStart = 10.dp, topEnd = 10.dp))
                        .fillMaxWidth()
                        .background(PrimaryDarkish)

                    i % 2 != 0 -> lightItemRowModifier
                    else -> darkItemRowModifier
                }
                item {
                    GetPropertyView(modifier = modifier, property = prop)
                }
            }
        }
    }

    @OptIn(ExperimentalMaterial3Api::class)
    @Composable
    fun Header() {
        val title by getQuestTitle().collectAsState(initial = "")
        CenterAlignedTopAppBar(
            colors = TopAppBarDefaults.topAppBarColors(containerColor = Primary),
            title = {
                Text(
                    title,
                    style = MaterialTheme.typography.headlineLarge,
                    modifier = Modifier.padding(vertical = 10.dp),
                    textAlign = TextAlign.Center,
                    color = SecondaryLight,
                    maxLines = 2,
                    overflow = TextOverflow.Ellipsis
                )
            },
            navigationIcon = {
                IconButton(onClick = { finish() }) {
                    Icon(
                        painter = painterResource(id = R.drawable.arrow_left),
                        contentDescription = "Back",
                        tint = SecondaryTextLight
                    )
                }
            },
            actions = {
                GetHeaderActions()
            },
        )
    }

    @Composable
    fun QuestItemRow(modifier: Modifier, @StringRes property: Int, value: String) {
        Row(modifier.padding(15.dp), verticalAlignment = Alignment.CenterVertically) {
            Text(
                stringResource(id = property),
                style = propertyTextStyle,
                textAlign = TextAlign.Start,
                modifier = Modifier.weight(1f)
            )
            Text(
                value,
                style = textStyle,
                textAlign = TextAlign.Start,
                modifier = Modifier.weight(1f)
            )
        }

    }

    @Composable
    fun QuestItemContentRow(modifier: Modifier, @StringRes property: Int, content: @Composable () -> Unit) {
        Row(modifier.padding(15.dp), verticalAlignment = Alignment.CenterVertically) {
            Text(
                stringResource(id = property),
                style = propertyTextStyle,
                textAlign = TextAlign.Start,
                modifier = Modifier.weight(1f)
            )
            Box(modifier = Modifier.weight(1f), contentAlignment = Alignment.CenterStart) {
                content()
            }
        }

    }

    @Composable
    fun LinkItem(onClick: () -> Unit) {
        Row(
            modifier = Modifier.clickable { onClick() },
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.spacedBy(5.dp)
        ) {
            Text(
                stringResource(id = R.string.link),
                style = textStyle,
                textAlign = TextAlign.Start
            )
            Image(
                painter = painterResource(id = R.drawable.ic_link),
                contentDescription = "Link",
                modifier = Modifier.size(24.dp, 24.dp)
            )
        }
    }

    protected fun parseDateOrDefault(date: String, @StringRes default: Int): String {
        return if (date.isNotEmpty()) {
            LocalDate.parse(date).format(formatter)
        } else {
            getString(default)
        }
    }

    protected fun addShortcut(quest: Quest) {
        if (ShortcutManagerCompat.isRequestPinShortcutSupported(this)) {
            with(quest) {
                val shortcutInfo = ShortcutInfoCompat.Builder(this@BaseQuestDetailActivity, title)
                    .setIntent(
                        getQuestManager().shortcutIntent(this@BaseQuestDetailActivity, this).setAction(Intent.ACTION_MAIN)
                    )
                    .setShortLabel(title)
                    .setIcon(IconCompat.createWithBitmap(Bitmap.createScaledBitmap(icon!!, 64, 64, true)))
                    .build()
                ShortcutManagerCompat.requestPinShortcut(this@BaseQuestDetailActivity, shortcutInfo, null)
            }
        } else {
            Toast.makeText(baseContext, getString(R.string.shortcut_are_not_supported), Toast.LENGTH_LONG).show()
        }
    }

    companion object {
        private val formatter = DateTimeFormatter.ofPattern("MMMM dd, yyyy")
        val textStyle = SolarusTypography.bodyLarge.copy(color = SecondaryTextLight, fontFamily = ubuntu_regular)
        private val propertyTextStyle = SolarusTypography.bodyLarge.copy(color = SecondaryTextDark)
        private val darkItemRowModifier = Modifier
            .padding(horizontal = 30.dp)
            .fillMaxWidth()
            .background(PrimaryDarkish)
        private val lightItemRowModifier = Modifier
            .padding(horizontal = 30.dp)
            .fillMaxWidth()
            .background(PrimaryMedium)
    }
}