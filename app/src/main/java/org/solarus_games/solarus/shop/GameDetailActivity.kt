package org.solarus_games.solarus.shop

import android.content.Context
import android.content.Intent
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.size
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.map
import org.solarus_games.solarus.BaseQuestDetailActivity
import org.solarus_games.solarus.R
import org.solarus_games.solarus.common.Controls
import org.solarus_games.solarus.common.Languages
import org.solarus_games.solarus.common.QuestProperties
import org.solarus_games.solarus.common.Ratings
import org.solarus_games.solarus.remote.models.Game
import org.solarus_games.solarus.theme.PrimaryLight
import org.solarus_games.solarus.theme.SecondaryTextLight
import org.solarus_games.solarus.utils.openUrlInExternalBrowser
import org.solarus_games.solarus.utils.parcelable
import javax.inject.Inject

@AndroidEntryPoint
class GameDetailActivity : BaseQuestDetailActivity() {

    @Inject
    lateinit var viewModel: GameDetailActivityViewModel

    lateinit var game: Game
    override fun getQuestManager() = viewModel.questManager

    override fun hasUpdate() = viewModel.localQuest.map { quest ->
        quest?.let {
            it.version != game.version
        } ?: run { false }
    }

    override fun onPlayClick() {
        viewModel.downloadAndLaunchGame(this, game) {
            if (it) finish()
        }
    }

    override fun onUpdateClick() {
        viewModel.downloadGame(this@GameDetailActivity, game)
    }

    override fun getQuestTitle() = MutableStateFlow(game.title)

    override fun getQuestDescription() = MutableStateFlow(game.excerpt)

    @Composable
    override fun GetHeaderActions() {
        Box {
            var menuExpanded by remember { mutableStateOf(false) }
            IconButton(onClick = { menuExpanded = !menuExpanded }) {
                Icon(
                    painter = painterResource(id = R.drawable.menu_dots),
                    contentDescription = "Menu Dots",
                    tint = SecondaryTextLight
                )
            }
            DropdownMenu(
                expanded = menuExpanded,
                onDismissRequest = { menuExpanded = false },
                modifier = Modifier.background(PrimaryLight)
            ) {
                val quest by viewModel.localQuest.collectAsState(null)
                quest?.let {
                    with(it) {
                        if (version != game.version) {
                            DropdownMenuItem(
                                text = {
                                    Text(stringResource(R.string.update))
                                },
                                onClick = {
                                    menuExpanded = false
                                    onUpdateClick()
                                },
                            )
                        }
                        DropdownMenuItem(
                            text = {
                                Text(stringResource(R.string.add_shortcut))
                            },
                            onClick = {
                                menuExpanded = false
                                addShortcut(it)
                            },
                        )
                        DropdownMenuItem(
                            text = {
                                Text(stringResource(R.string.remove))
                            },
                            onClick = {
                                menuExpanded = false
                                viewModel.deleteGame(game)
                            },
                        )
                    }
                } ?: run {
                    DropdownMenuItem(
                        text = {
                            Text(stringResource(R.string.download))
                        },
                        onClick = {
                            menuExpanded = false
                            viewModel.downloadGame(this@GameDetailActivity, game)
                        },
                    )
                }
            }
        }
    }

    @Composable
    override fun GetThumbnailView() {
        GameThumbnailView(modifier = Modifier.fillMaxWidth(), game, true) {}
    }

    override fun setup() {
        game = requireNotNull(intent.parcelable(ARG_GAME))
        viewModel.updateGameInfo(game)
    }

    override fun getProperties() = MutableStateFlow(
        listOf(
            QuestProperties.DEVELOPER,
            QuestProperties.RELEASE_DATE,
            QuestProperties.LAST_UPDATE,
            QuestProperties.LICENSES,
            QuestProperties.AGE_RATING,
            QuestProperties.LANGUAGE,
            QuestProperties.CONTROLS,
            QuestProperties.GENRE,
            QuestProperties.VERSION,
//        QuestProperties.PLATFORM,
//        QuestProperties.SOURCE_CODE,
            QuestProperties.WEBSITE
        )
    )

    @Composable
    override fun GetPropertyView(modifier: Modifier, property: QuestProperties) {
        with(game) {
            when (property) {
                QuestProperties.DEVELOPER -> QuestItemRow(
                    modifier,
                    property = R.string.developer,
                    value = developer.joinToString(", ")
                )

                QuestProperties.RELEASE_DATE -> QuestItemRow(
                    modifier,
                    property = R.string.release_date,
                    value = parseDateOrDefault(initialReleaseDate, R.string.no_release_date)
                )

                QuestProperties.LAST_UPDATE -> QuestItemRow(
                    modifier,
                    property = R.string.latest_update_date,
                    value = parseDateOrDefault(latestUpdateDate, R.string.no_update_date)
                )

                QuestProperties.LICENSES -> QuestItemRow(
                    modifier,
                    property = R.string.license,
                    value = licenses.joinToString(", ")
                )

                QuestProperties.AGE_RATING -> QuestItemContentRow(modifier, R.string.age_rating) {
                    Row(verticalAlignment = Alignment.CenterVertically, horizontalArrangement = Arrangement.spacedBy(10.dp)) {
                        Image(
                            painter = painterResource(id = R.drawable.rating_all),
                            contentDescription = "Rating",
                            modifier = Modifier.size(24.dp, 24.dp)
                        )
                        Text(
                            stringResource(Ratings.valueOf(age.uppercase()).value),
                            style = textStyle,
                        )
                    }
                }

                QuestProperties.LANGUAGE -> QuestItemContentRow(modifier, R.string.languages) {
                    Column(verticalArrangement = Arrangement.spacedBy(5.dp)) {
                        languages.chunked(5).forEach { chunk ->
                            Row(
                                verticalAlignment = Alignment.CenterVertically,
                                horizontalArrangement = Arrangement.spacedBy(5.dp)
                            ) {
                                chunk.forEach {
                                    Image(
                                        painter = painterResource(id = Languages.valueOf(it.uppercase()).icon),
                                        contentDescription = "Lang $it",
                                        modifier = Modifier.size(24.dp, 24.dp)
                                    )
                                }
                            }
                        }
                    }
                }

                QuestProperties.CONTROLS -> QuestItemContentRow(modifier, R.string.controls) {
                    Row(verticalAlignment = Alignment.CenterVertically, horizontalArrangement = Arrangement.spacedBy(5.dp)) {
                        controls.forEach {
                            Image(
                                painter = painterResource(id = Controls.valueOf(it.uppercase()).icon),
                                contentDescription = "Controls $it",
                                modifier = Modifier.size(24.dp, 24.dp)
                            )
                        }
                    }
                }

                QuestProperties.GENRE -> QuestItemRow(
                    modifier,
                    property = R.string.genre,
                    value = genre.joinToString(", ")
                )

                QuestProperties.VERSION -> QuestItemRow(
                    modifier,
                    property = R.string.version,
                    value = version
                )

//                QuestProperties.PLATFORM -> GameItemRow(
//                    modifier,
//                    property = R.string.platforms,
//                    value = platforms.joinToString(", ")
//                )

//                QuestProperties.SOURCE_CODE -> GameItemContentRow(modifier, property = R.string.source_code) {
//                    LinkItem {
//                        // TODO source code?
//                    }
//                }

                QuestProperties.WEBSITE -> QuestItemContentRow(modifier, property = R.string.website) {
                    LinkItem {
                        openUrlInExternalBrowser(website)
                    }
                }

                else -> {}
            }
        }
    }

    companion object {
        private const val ARG_GAME = "game"

        fun startActivity(context: Context, game: Game) {
            val intent = Intent(context, GameDetailActivity::class.java).also {
                it.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            }
            intent.putExtra(ARG_GAME, game)
            context.startActivity(intent)
        }
    }
}