package org.solarus_games.solarus.theme

import androidx.compose.ui.graphics.Color

val Primary = Color(0xFF2E1B66)
val PrimaryLight = Color(0xFF593BB0)
val PrimaryMedium = Color(0xFF28165C)
val PrimaryDarkish = Color(0xFF23164C)
val Primary3 = Color(0xFF4A2C94)
val PrimaryDark = Color(0xFF1b104d)
val Secondary = Color(0xFFFF7B00)
val SecondaryLight = Color(0xFFFFBA00)
val PrimaryText = Color(0xFFFFFFFF)
val SecondaryText = Color(0xFFB79FFF)
val SecondaryTextLight = Color(0xFFC9BEEC)
val SecondaryTextDark = Color(0xFF7458C3)
val TextTitle = Color(0xFF404040)
val TextSubTitle = Color(0xFF9a9a9a)
val TextColor = Color(0xFF9a9a9a)
val TextLabel = Color(0xFF404040)
val ErrorColor = Color(0xFFFF5888)
val Success = Color(0xFF0DC08A)