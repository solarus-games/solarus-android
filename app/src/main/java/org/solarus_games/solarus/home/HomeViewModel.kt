package org.solarus_games.solarus.home

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import org.solarus_games.solarus.quest.Quest
import org.solarus_games.solarus.quest.QuestManager
import org.solarus_games.solarus.recent.RecentlyPlayedDao
import org.solarus_games.solarus.remote.models.HomeScreen
import org.solarus_games.solarus.remote.use_cases.GetHomeUseCase
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class HomeViewModel @Inject constructor(
    private val getHomeUseCase: GetHomeUseCase,
    private val questManager: QuestManager,
) : ViewModel() {

    private val _state = MutableStateFlow(HomeScreen(loading = true))
    val state = _state.asStateFlow()

    init {
        getHomeScreen()
    }

    fun getHomeScreen(useCache: Boolean = true) {
        getHomeUseCase(useCache).onEach { homeScreen ->
            _state.emit(homeScreen)
        }.launchIn(viewModelScope)
    }

    fun onQuestSelected(context: Context, quest: Quest) {
        questManager.onQuestDetailClick(context, quest)
    }

    fun hasQuests() = questManager.quests.value.isNotEmpty()
}