package org.solarus_games.solarus.joypad;

import static org.solarus_games.solarus.utils.UiHelperKt.setLayoutPosition;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import androidx.activity.OnBackPressedCallback;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.android.material.navigation.NavigationView;

import org.solarus_games.solarus.R;
import org.solarus_games.solarus.joypad.ButtonMappingManager.InputLayout;

import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;

@AndroidEntryPoint
public class ButtonMappingActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    DrawerLayout drawer;
    ViewGroup layoutManager;
    List<VirtualButton> layoutList;
    static ButtonMappingActivity sActivity;

    @Inject
    ButtonMappingManager buttonMappingManager;
    InputLayout inputLayout;


    public static final String TAG_ID = "id";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_button_mapping);

        sActivity = this;


        getOnBackPressedDispatcher().addCallback(this, new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                openOrCloseMenu();
            }
        });


        // Menu configuration
        this.drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        // Hide the status bar
        hideStatusBar();

        layoutManager = (RelativeLayout) findViewById(R.id.button_mapping_activity_layout);

        //Retrive the InputLayout to work with
        Intent intent = getIntent();
        int id = intent.getIntExtra(TAG_ID, 0);
        inputLayout = buttonMappingManager.getLayoutById(id);

        //We does a copy of the inputLayout's button list
        layoutList = new LinkedList<>();
        VirtualButton vb = null;
        for (VirtualButton b : inputLayout.getButtonList()) {
            if (b instanceof VirtualCross) {
                vb = new VirtualCross(this, b.getPosX(), b.getPosY(), b.getSize());
            } else if (b.keyCode > 0) {
                vb = VirtualButton.Create(this, b.getKeyCode(), b.getPosX(), b.getPosY(), b.getSize());
            }
            vb.setDebug_mode(true);
            layoutList.add(vb);
        }
        drawButtons();
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.button_mapping_menu_add_button:
                showSupportedButton();
                openOrCloseMenu();
                break;
            case R.id.button_mapping_menu_reset:
                layoutList = InputLayout.getDefaultInputLayout(this).getButtonList();
                drawButtons();
                openOrCloseMenu();
                break;
            case R.id.button_mapping_menu_exit_without_saving:
                this.finish();
                break;
            case R.id.button_mapping_menu_save_and_quit:
                save();
                this.finish();
                break;
            default:
                return false;
        }
        return true;
    }

    public void openOrCloseMenu() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            drawer.openDrawer(GravityCompat.START);
        }
    }

    public void hideStatusBar() {
        // Hide the status bar
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    public void save() {
        //Copy the button from layoutList to the InputLayout
        inputLayout.getButtonList().clear();
        for (VirtualButton b : layoutList) {
            if (b instanceof VirtualCross) {
                inputLayout.getButtonList().add(new VirtualCross(this, b.getPosX(), b.getPosY(), b.getSize()));
            } else if (b.keyCode > 0) {
                inputLayout.getButtonList().add(VirtualButton.Create(this, b.getKeyCode(), b.getPosX(), b.getPosY(), b.getSize()));
            }
        }

        //Save the ButtonMappingModel
        buttonMappingManager.save();
    }

    interface KeyCodeListener {
        void onChoose(int keyCode);
    }

    public void chooseButton(final KeyCodeListener l) {
        List<String> items = buttonMappingManager.generateKeyCodeStrings();
        final List<Integer> itemsCodes = buttonMappingManager.generateKeyCodes();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getResources().getString(R.string.add_a_button));
        builder.setItems(items.toArray(new String[items.size()]), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                l.onChoose(itemsCodes.get(item));
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void showSupportedButton() {
        chooseButton(new KeyCodeListener() {
            @Override
            public void onChoose(int keyCode) {
                addAButton(keyCode);
            }
        });
    }

    public void addAButton(int keyCode) {

        Context ctx = getApplicationContext();

        VirtualButton vb = null;
        if (keyCode > 0) {
            vb = VirtualButton.Create(this, keyCode, 0.5, 0.5, 100);
        }
        vb.setDebug_mode(true);
        layoutList.add(vb);
        drawButtons();
    }

    /**
     * Draws all buttons.
     */
    private void drawButtons() {
        layoutManager.removeAllViews();
        for (VirtualButton b : layoutList) {
            b.setDebug_mode(true);
            setLayoutPosition(this, b, b.getPosX(), b.getPosY());
            layoutManager.addView(b);
        }
    }

    public static void dragVirtualButton(VirtualButton v, MotionEvent event) {
        float x, y;

        int action = event.getActionMasked();

        switch (action) {
            case (MotionEvent.ACTION_DOWN):
            case (MotionEvent.ACTION_MOVE):
                // Calculation of the new view position
                x = (v.getLeft() + event.getX() - v.getWidth() / 2)
                        / v.getResources().getDisplayMetrics().widthPixels;
                y = (v.getTop() + event.getY() - v.getHeight() / 2)
                        / v.getResources().getDisplayMetrics().heightPixels;

                setLayoutPosition((AppCompatActivity) v.getContext(), v, x, y);

                v.setPosX(x);
                v.setPosY(y);

                return;
            default:
        }
    }

    public void removeButton(VirtualButton v) {
        layoutList.remove(v);
        drawButtons();
    }

    public void changeButtonCode(final VirtualButton v) {
        chooseButton(new KeyCodeListener() {
            @Override
            public void onChoose(int keyCode) {
                v.setKeyCode(keyCode);
                v.invalidate();
            }
        });
    }

    public static void virtualButtonLongPress(final VirtualButton v) {
        Context ctx = v.getContext();
        AlertDialog.Builder builder = new AlertDialog.Builder(sActivity);
        builder.setTitle(ctx.getResources().getString(R.string.modify_button));
        final CharSequence[] items = {
                ctx.getString(R.string.change_button_key),
                ctx.getString(R.string.remove_button),
        };
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                switch (item) {
                    case 0:
                        sActivity.changeButtonCode(v);
                        break;
                    case 1:
                        sActivity.removeButton(v);
                        break;
                }
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    /**
     * Called after a screen orientation changement
     */
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        // We draw the button again to match the positions
        drawButtons();
    }
}
