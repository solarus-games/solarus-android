package org.solarus_games.solarus.remote.use_cases

import android.content.Context
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow
import org.solarus_games.solarus.R
import org.solarus_games.solarus.common.Logger
import org.solarus_games.solarus.common.Resource
import org.solarus_games.solarus.quest.Quest
import org.solarus_games.solarus.quest.QuestManager
import org.solarus_games.solarus.remote.DownloadingGameDialog
import org.solarus_games.solarus.remote.RemoteRepository
import org.solarus_games.solarus.remote.models.Game
import org.solarus_games.solarus.utils.getSupportFragmentManager
import javax.inject.Inject

class DownloadGameUseCase @Inject constructor(
    private val remoteRepository: RemoteRepository,
    private val questManager: QuestManager,
    private val logger: Logger
) {

    var dialog: DownloadingGameDialog? = null
    operator fun invoke(context: Context, game: Game): Flow<Resource<Quest>> = flow {
        logger.d("DownloadGameUseCase", "Downloading: ${game.title}, url: ${game.download}")
        dialog = DownloadingGameDialog.newInstance()
        context.getSupportFragmentManager()?.let {
            dialog?.show(it, DownloadingGameDialog.TAG)
        }
        emit(Resource.Loading())
        var quest: Quest? = null
        if (remoteRepository.downloadGame(game)) {
            if (questManager.isRemoteQuestDownloaded(game.id)) {
                quest = questManager.fromPath(questManager.getRemoteQuestFile(game.id).absolutePath)
            }
        }
        if (quest != null) {
            emit(Resource.Success(quest))
        } else {
            emit(Resource.Error(message = context.getString(R.string.game_download_error, game.title)))
        }
        dialog?.dismissAllowingStateLoss()
        dialog = null
    }.catch { e ->
        logger.e("DownloadGameUseCase", "Error", e)
        emit(Resource.Error(message = context.getString(R.string.game_download_error, game.title)))
        dialog?.dismissAllowingStateLoss()
        dialog = null
    }
}