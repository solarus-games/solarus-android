# LuaJIT

Building LuaJIT requires two steps supported by scripts in this directory:

1. Prepare the LuaJIT source code directory to build from (`prepare.sh`).
2. Build LuaJIT for all supported Android ABIs (`build.sh`).

## Prepare LuaJIT directory

To prepare the LuaJIT source code repository, you need `git` and `tar` installed.

> **Note:** LuaJIT uses a rolling release model for the `v2.1` branch.

Run the `prepare.sh` script to prepare the `luajit/` directory for building:
```
$ ./prepare.sh
```

This script clones and exports the upstream LuaJIT repository at the `HEAD` of the `v2.1` branch.
This workflow is required because the LuaJIT repository contains dynamic files such as `/.relver`
that are resolved and generated during a repository export operation.

## Build LuaJIT

To build LuaJIT for Android, you need `gcc`, `gcc-multilib`, `make` and the
[Android NDK](https://developer.android.com/ndk/downloads).

> **Note:** In Debian/Ubuntu, the `build-essential` and `gcc-multilib` packages are sufficient.

> **Note:** Make sure to use the same version of Android NDK used in the Solarus Android project.

To facilitate this step, the
[Solarus Android build image](https://gitlab.com/solarus-games/solarus-devops/-/tree/master/android-build-env?ref_type=heads)
can be used as well.

The following example illustrates how to build LuaJIT using the Solarus Android build image:
```
$ docker run --rm -it -v $PWD:/work solarus/android-build-env:latest
# apt-get update
# apt-get install --no-install-recommends build-essential gcc-multilib
# cd /work
# rm -rf prebuilt
# NDK_ROOT=$ANDROID_HOME/ndk/26.3.11579264 ./build.sh
```
The default `prebuilt` directory must be removed each time a new build is performed.

The Solarus Android build container runs as the `root` user and will create files owned by `root`.
After the build finishes, you should ensure that the created files in the host, outside of the
build container, are owned by the regular user where the repository was cloned.
