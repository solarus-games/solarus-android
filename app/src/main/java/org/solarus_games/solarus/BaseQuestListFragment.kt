package org.solarus_games.solarus

import androidx.annotation.StringRes
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.pullrefresh.PullRefreshIndicator
import androidx.compose.material.pullrefresh.pullRefresh
import androidx.compose.material.pullrefresh.rememberPullRefreshState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import org.solarus_games.solarus.preferences.PreferencesManager

abstract class BaseQuestListFragment<T> : NowPlayingFragment() {

    @get:StringRes
    abstract val getHeaderStringRes: Int

    abstract fun getPreferencesManager(): PreferencesManager

    abstract fun shouldFilter(search: String, item: T): Boolean

    abstract fun handleRefresh()

    @Composable
    abstract fun GetItemView(item: T)

    @Composable
    abstract fun GetErrorView()

    private val searchFlow = MutableStateFlow("")

    @Composable
    override fun GetHeader() {
        val search by searchFlow.collectAsState()
        QuestHeader(getHeaderStringRes, getPreferencesManager(), search) {
            searchFlow.value = it
        }
    }

    @OptIn(ExperimentalMaterialApi::class)
    @Composable
    fun QuestListView(items: List<T>) {
        if (items.isNotEmpty()) {
            val refreshScope = rememberCoroutineScope()
            var refreshing by remember { mutableStateOf(false) }

            fun refresh() = refreshScope.launch {
                refreshing = true
                handleRefresh()
                refreshing = false
            }

            val state = rememberPullRefreshState(refreshing, ::refresh)
            Box(Modifier.pullRefresh(state)) {
                QuestColumnView(items)
                PullRefreshIndicator(refreshing, state, Modifier.align(Alignment.TopCenter))
            }
        } else {
            GetErrorView()
        }
    }

    @Composable
    fun QuestColumnView(items: List<T>) {
        val search by searchFlow.collectAsState()
        val filteredList = remember(search, items) { items.filter { item -> shouldFilter(search, item) } }
        LazyColumn(
            modifier = Modifier
                .fillMaxSize(),
            verticalArrangement = Arrangement.spacedBy(20.dp),
            contentPadding = PaddingValues(vertical = 20.dp)
        ) {
            items(filteredList) { item ->
                GetItemView(item)
            }
        }
    }
}