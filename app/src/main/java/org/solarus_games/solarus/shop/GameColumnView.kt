package org.solarus_games.solarus.shop

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.unit.dp
import org.solarus_games.solarus.remote.models.Game
import org.solarus_games.solarus.theme.SecondaryText
import org.solarus_games.solarus.theme.SecondaryTextLight
import org.solarus_games.solarus.theme.SolarusTypography

@Composable
fun GameColumnView(
    modifier: Modifier,
    game: Game,
    onGameClick: () -> Unit
) {
    with(game) {
        Row(
            modifier.clickable {
                onGameClick()
            },
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.spacedBy(20.dp)
        ) {
            GameThumbnailView(
                modifier = Modifier
                    .clip(RoundedCornerShape(5.dp))
                    .size(90.dp, 48.dp), game = game
            ) {
                onGameClick()
            }
            Column(
                modifier = Modifier.weight(1f),
                horizontalAlignment = Alignment.Start,
                verticalArrangement = Arrangement.spacedBy(5.dp)
            ) {
                Text(title, style = SolarusTypography.bodyLarge, maxLines = 2, color = SecondaryTextLight)
                Text(developer.joinToString(", "), style = SolarusTypography.labelSmall, maxLines = 1, color = SecondaryText)
            }
        }
    }
}