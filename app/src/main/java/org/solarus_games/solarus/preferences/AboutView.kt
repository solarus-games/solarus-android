package org.solarus_games.solarus.preferences

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import org.solarus_games.solarus.R
import org.solarus_games.solarus.common.Constants.CHANGE_LOG_URL
import org.solarus_games.solarus.common.Constants.REPORT_BUG_URL
import org.solarus_games.solarus.common.Constants.SETTING_SPACING
import org.solarus_games.solarus.common.Constants.SOLARUS_BASE_URL
import org.solarus_games.solarus.common.Constants.SOURCE_CODE_URL
import org.solarus_games.solarus.common.Preference
import org.solarus_games.solarus.theme.SecondaryTextLight
import org.solarus_games.solarus.utils.openUrlInExternalBrowser

@Composable
fun AboutView() {
    val context = LocalContext.current

    LazyColumn(
        modifier = Modifier
            .fillMaxSize()
            .background(MaterialTheme.colorScheme.background)
            .padding(horizontal = 20.dp),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.spacedBy(SETTING_SPACING.dp),
        contentPadding = PaddingValues(vertical = 20.dp)
    ) {

        item {
            Column(
                modifier = Modifier.fillMaxWidth(),
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.spacedBy(15.dp)
            ) {
                Image(
                    painter = painterResource(id = R.drawable.solarus_logo_white),
                    contentDescription = "Icon",
                    Modifier
                        .height(48.dp)
                        .testTag("Icon")
                )

                Text(
                    stringResource(id = R.string.solarus_description),
                    textAlign = TextAlign.Center,
                    fontFamily = FontFamily.Serif,
                    maxLines = 2,
                    fontSize = 18.sp,
                    fontWeight = FontWeight.Thin,
                    color = SecondaryTextLight
                )

                Text(
                    stringResource(id = R.string.solarus_copyright),
                    textAlign = TextAlign.Center,
                    fontFamily = FontFamily.Serif,
                    maxLines = 1,
                    fontSize = 18.sp,
                    fontWeight = FontWeight.Bold,
                    color = SecondaryTextLight
                )
                Text(
                    stringResource(id = R.string.program_license),
                    textAlign = TextAlign.Center,
                    fontFamily = FontFamily.Serif,
                    maxLines = 1,
                    fontSize = 12.sp,
                    fontWeight = FontWeight.Thin,
                    color = SecondaryTextLight
                )
                Text(
                    stringResource(id = R.string.asset_license),
                    textAlign = TextAlign.Center,
                    fontFamily = FontFamily.Serif,
                    maxLines = 1,
                    fontSize = 12.sp,
                    fontWeight = FontWeight.Thin,
                    color = SecondaryTextLight
                )
                Image(
                    painter = painterResource(id = R.drawable.license),
                    contentDescription = "Copy Icon",
                    Modifier
                        .height(48.dp)
                        .testTag("con")
                )
            }
        }


        item {
            Preference(
                title = R.string.version,
                message = stringResource(id = R.string.app_version)
            ) {}
        }

        item {
            Preference(
                title = R.string.change_log,
                message = stringResource(id = R.string.see_changes)
            ) {
                context.openUrlInExternalBrowser(CHANGE_LOG_URL)
            }
        }

        item {
            Preference(
                title = R.string.source_code,
                message = stringResource(id = R.string.source_code_sum)
            ) {
                context.openUrlInExternalBrowser(SOURCE_CODE_URL)
            }
        }

        item {
            Preference(
                title = R.string.report_bug,
                message = stringResource(id = R.string.report_bug_sum)
            ) {
                context.openUrlInExternalBrowser(REPORT_BUG_URL)
            }
        }

        item {
            Preference(
                title = R.string.view_website,
                message = stringResource(id = R.string.view_website_sum)
            ) {
                context.openUrlInExternalBrowser(SOLARUS_BASE_URL)
            }
        }
    }
}
