package org.solarus_games.solarus.preferences

import android.content.Context
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Button
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import org.solarus_games.solarus.R
import org.solarus_games.solarus.common.Constants.BUTTON_TEXT_PADDING
import org.solarus_games.solarus.common.PreferenceDivider
import org.solarus_games.solarus.common.PreferenceHeader
import org.solarus_games.solarus.joypad.ButtonMappingManager.InputLayout
import org.solarus_games.solarus.joypad.ButtonMappingManager.InputLayout.Companion.getDefaultButtonList
import org.solarus_games.solarus.theme.PrimaryText

@Composable
fun JoypadPreference(modifier: Modifier, viewModel: JoypadPreferenceViewModel = hiltViewModel()) {
    val inputs by viewModel.inputs.collectAsState(emptyList())
    val context = LocalContext.current
    Column(modifier = modifier.padding(bottom = 10.dp), verticalArrangement = Arrangement.spacedBy(5.dp)) {
        PreferenceDivider(title = R.string.layouts)

        inputs.forEach { input ->
            JoypadInputItem(input = input)
        }

        Button(onClick = {
            val input = EditText(context)
            // The dialog
            val builder = AlertDialog.Builder(context)
            builder.setTitle(R.string.add_joypad_layout).setView(input)
                .setPositiveButton(R.string.ok) { _, _ ->
                    val text = input.text.toString()
                    if (text.isNotEmpty()) {
                        // Create a new input layout, with the default layout
                        val newInputLayout = InputLayout(text)
                        newInputLayout.buttonList = getDefaultButtonList(context)

                        // Add it to the input layout list, and open the activity to modify it
                        viewModel.addNewInputLayout(newInputLayout)
                        viewModel.editInputLayout(context, newInputLayout)
                    }
                }.setNegativeButton("Cancel") { dialog, _ -> dialog.cancel() }
            builder.show()
        }, modifier = Modifier.fillMaxWidth()) {
            Text(text = stringResource(id = R.string.add_joypad_layout), modifier = Modifier.padding(BUTTON_TEXT_PADDING.dp))
        }
    }
}

@Composable
fun JoypadInputItem(input: InputLayout, viewModel: JoypadPreferenceViewModel = hiltViewModel()) {
    val defaultId by viewModel.defaultId.collectAsState(0)
    val context = LocalContext.current
    Row(
        Modifier
            .fillMaxWidth()
            .clickable { viewModel.editInputLayout(context, input) },
        horizontalArrangement = Arrangement.SpaceBetween,
        verticalAlignment = Alignment.CenterVertically
    ) {
        val title = if (input.id == defaultId) "${input.name} (${stringResource(id = R.string.default_layout)})" else input.name
        PreferenceHeader(title)
        Icon(painter = painterResource(id = R.drawable.ic_menu),tint = PrimaryText, contentDescription = "${input.name} menu", modifier = Modifier
            .padding(10.dp)
            .clickable {
                val builder = AlertDialog.Builder(context)

                val c: Context = context
                val choiceArray = arrayOf(
                    c.getString(R.string.set_as_default), c.getString(R.string.edit_name),
                    c.getString(R.string.edit_layout), c.getString(R.string.delete)
                )

                builder
                    .setTitle(input.name)
                    .setItems(choiceArray) { _, which ->
                        when (which) {
                            0 -> viewModel.setAsDefaultLayout(input.id)
                            1 -> viewModel.editInputLayoutName(context, input)
                            2 -> viewModel.editInputLayout(context, input)
                            3 -> viewModel.deleteInputLayout(context, input)
                            else -> {}
                        }
                    }

                builder.show()
            })
    }
}

