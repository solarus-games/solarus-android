package org.solarus_games.solarus.quest

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import org.solarus_games.solarus.R

@Composable
fun QuestIconView(
    modifier: Modifier,
    quest: Quest,
    isLarge: Boolean = false,
    questManagerViewModel: QuestManagerViewModel = hiltViewModel(),
    onQuestClick: () -> Unit
) {
    val currentQuest by questManagerViewModel.currentQuest().collectAsState(null)
    with(quest) {
        Card(
            onClick = {
                onQuestClick()
            },
            shape = RectangleShape,
            elevation = CardDefaults.cardElevation(
                defaultElevation = 15.dp,
                pressedElevation = 0.dp,
                hoveredElevation = 8.dp
            ),
            modifier = modifier
        ) {
            Box(contentAlignment = Alignment.BottomCenter) {
                icon?.let {
                    Image(
                        bitmap = it.asImageBitmap(),
                        contentDescription = title,
                        contentScale = ContentScale.FillHeight,
                        modifier = modifier
                    )
                } ?: run {
                    Image(
                        painterResource(id = R.drawable.placeholder_thumbnail),
                        contentDescription = title,
                        contentScale = ContentScale.FillHeight,
                        modifier = modifier,
                    )
                }
                if (quest.path == currentQuest?.path) {
                    NowPlayingThumbnailView(isLarge)
                }
            }
        }
    }
}