package org.solarus_games.solarus.remote.cache

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.solarus_games.solarus.common.Constants.moshi
import org.solarus_games.solarus.common.Logger
import org.solarus_games.solarus.preferences.PreferencesManager
import org.solarus_games.solarus.remote.models.GameResponse
import org.solarus_games.solarus.utils.isValidTimestamp
import javax.inject.Inject

class GamesCache @Inject constructor(
    private val logger: Logger,
    private val preferencesManager: PreferencesManager
) {

    private var memoryCache: GameResponse? = null


    fun getGames(): GameResponse? {
        if (memoryCache != null && memoryCache!!.timestamp.isValidTimestamp()) {
            logger.d(TAG, "memoryCache VALID")
            return memoryCache
        }
        logger.d(TAG, "memoryCache INVALID")
        preferencesManager.getString(KEY)?.let { diskCacheString ->
            val diskCache = moshi<GameResponse>().fromJson(diskCacheString)
            if (diskCache != null && diskCache.timestamp.isValidTimestamp()) {
                logger.d(TAG, "diskCache VALID")
                memoryCache = diskCache
                return diskCache
            }
            logger.d(TAG, "diskCache INVALID")
        } ?: run {
            logger.d(TAG, "diskCache INVALID")
        }
        logger.d(TAG, "NO CACHE")
        return null
    }

    suspend fun setGames(response: GameResponse) = withContext(Dispatchers.IO) {
        memoryCache = response
        preferencesManager.setString(KEY, moshi<GameResponse>().toJson(response))
    }


    companion object {
        private const val TAG = "GamesCache"
        private const val KEY = "games_cache"
    }
}