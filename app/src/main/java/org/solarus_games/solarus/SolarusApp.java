package org.solarus_games.solarus;

import android.app.Application;

import dagger.hilt.android.HiltAndroidApp;

@HiltAndroidApp
public class SolarusApp extends Application {}
