package org.solarus_games.solarus.preferences

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement.spacedBy
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Clear
import androidx.compose.material.icons.outlined.Info
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.codekidlabs.storagechooser.StorageChooser
import dagger.hilt.android.AndroidEntryPoint
import org.solarus_games.solarus.R
import org.solarus_games.solarus.common.ComposeDialog
import org.solarus_games.solarus.theme.ErrorColor
import org.solarus_games.solarus.theme.Primary3
import org.solarus_games.solarus.theme.PrimaryLight
import org.solarus_games.solarus.theme.Secondary
import org.solarus_games.solarus.theme.SecondaryLight
import org.solarus_games.solarus.theme.SecondaryText
import org.solarus_games.solarus.theme.SolarusTypography
import org.solarus_games.solarus.theme.Success
import javax.inject.Inject

@AndroidEntryPoint
class FolderPreferenceFragment : ComposeDialog() {

    @Inject
    lateinit var viewModel: FolderPreferenceViewModel

    @Composable
    override fun GetContent() {
        val keyboardController = LocalSoftwareKeyboardController.current
        val state by viewModel.state.collectAsState()
        with(state) {
            val isValid = status != PathStatus.INVALID
            val color = if (isValid) Success else ErrorColor
            Column(
                Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 10.dp),
                horizontalAlignment = Alignment.End,
                verticalArrangement = spacedBy(15.dp)
            ) {

                Text(
                    stringResource(id = R.string.chose_quest_folder_title),
                    style = SolarusTypography.headlineSmall,
                    color = SecondaryLight,
                    textAlign = TextAlign.Start,
                    modifier = Modifier.fillMaxWidth()
                )
                Text(
                    stringResource(id = R.string.chose_quest_folder_message),
                    style = SolarusTypography.bodyLarge,
                    color = SecondaryText,
                    textAlign = TextAlign.Start,
                    modifier = Modifier.fillMaxWidth()
                )
                Column(verticalArrangement = spacedBy(5.dp)) {
                    TextField(
                        modifier = Modifier.fillMaxWidth(),
                        label = { Text(stringResource(id = R.string.folder_path), color = SecondaryText) },
                        trailingIcon = {
                            IconButton(onClick = { viewModel.updatePath("") }) {
                                Icon(imageVector = Icons.Default.Clear, contentDescription = "Clear Dir")
                            }
                        },
                        maxLines = 1,
                        value = folderPath,
                        keyboardOptions = KeyboardOptions(
                            imeAction = ImeAction.Done,
                        ),
                        keyboardActions = KeyboardActions(
                            onDone = { keyboardController?.hide() }
                        ),
                        onValueChange = {
                            viewModel.updatePath(it)
                        },
                        colors = TextFieldDefaults.colors(
                            focusedTrailingIconColor = SecondaryText,
                            unfocusedTrailingIconColor = SecondaryText,
                            focusedContainerColor = Primary3,
                            unfocusedContainerColor = Primary3,
                            focusedIndicatorColor = color,
                            unfocusedIndicatorColor = color,
                        )
                    )

                    Row(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(horizontal = 10.dp),
                        verticalAlignment = Alignment.CenterVertically,
                        horizontalArrangement = spacedBy(5.dp)
                    ) {
                        Icon(
                            imageVector = if (isValid) Icons.Outlined.Info else Icons.Default.Clear,
                            contentDescription = "Invalid Dir",
                            tint = color,
                            modifier = Modifier
                                .size(16.dp),
                        )
                        Text(
                            text = stringResource(id = status.message),
                            color = color,
                            style = SolarusTypography.labelSmall,
                        )
                    }
                }

                Button(onClick = {
                    val chooser = StorageChooser.Builder()
                        .withActivity(activity)
                        .allowCustomPath(true)
                        .withFragmentManager(requireActivity().fragmentManager)
                        .setType(StorageChooser.DIRECTORY_CHOOSER)
                        .build()

                    chooser.setOnSelectListener { path ->
                        viewModel.updatePath(path)
                        viewModel.setQuestDir()
                        dismissAllowingStateLoss()
                    }
                    chooser.show()

                }, colors = ButtonDefaults.buttonColors(containerColor = PrimaryLight)) {
                    Image(
                        painter = painterResource(id = R.drawable.folder_open), contentDescription = "Browse Folders",
                        modifier = Modifier
                            .size(24.dp),
                        colorFilter = ColorFilter.tint(Color.White)
                    )

                    Text(
                        stringResource(id = R.string.browse),
                        Modifier.padding(start = 5.dp),
                    )
                }

                Row(horizontalArrangement = spacedBy(20.dp)) {
                    Button(onClick = {
                        dismissAllowingStateLoss()
                    }, colors = ButtonDefaults.buttonColors(containerColor = PrimaryLight)) {
                        Text(stringResource(id = R.string.cancel), Modifier.padding(start = 5.dp))
                    }

                    Button(
                        onClick = {
                            viewModel.setQuestDir()
                            dismissAllowingStateLoss()
                        },
                        enabled = isValid,
                        colors = ButtonDefaults.buttonColors(containerColor = Secondary)
                    ) {
                        Text(stringResource(id = R.string.ok), Modifier.padding(start = 5.dp))
                    }
                }
            }
        }
    }

    companion object {
        const val TAG = "FolderPrefs"

        fun newInstance(): FolderPreferenceFragment {
            return FolderPreferenceFragment()
        }
    }
}