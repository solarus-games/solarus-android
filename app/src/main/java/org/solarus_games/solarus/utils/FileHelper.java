package org.solarus_games.solarus.utils;

import android.content.Context;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.json.JSONException;
import org.json.JSONObject;

public class FileHelper {

    public static JSONObject readJSON(String contentFile) {
        try {
            // Parse the JSON
            JSONObject jso = new JSONObject(contentFile);
            return jso;
        } catch (JSONException e) {
            Log.e("JSO reading", "Error parsing a JSO file : " + e.getMessage());
        }

        return null;
    }

    public static JSONObject readJSONFile(String path) {
        String file = new String(), tmp;
        try {
            // Read the file
            BufferedReader bf = new BufferedReader(new FileReader(new File(path)));
            while ((tmp = bf.readLine()) != null) {
                file += tmp;
            }
            bf.close();

            // Parse the JSON
            JSONObject jso = new JSONObject(file);
            return jso;
        } catch (JSONException e) {
            Log.e("JSO reading", "Error parsing the JSO file " + path + "\n" + e.getMessage());
        } catch (IOException e) {
            Log.e("JSO reading", "Error reading the file " + path + "\n" + e.getMessage());
        }

        return null;
    }

    public static String readInternalFileContent(Context content, String fileName) {
        String file = new String(), tmp;
        try {
            // Read the file
            BufferedReader bf = new BufferedReader(new InputStreamReader(content.openFileInput(fileName)));
            while ((tmp = bf.readLine()) != null) {
                file += tmp;
            }
            bf.close();
        } catch (IOException e) {
            Log.e("JSO reading", "Error reading the file " + fileName + "\n" + e.getMessage());
        }
        return file;
    }
}
