package org.solarus_games.solarus.preferences

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import org.solarus_games.solarus.R
import org.solarus_games.solarus.common.Constants.SETTING_SPACING
import org.solarus_games.solarus.common.Preference

@Composable
fun SettingsView(onPrefPositionClicked: (position: Int) -> Unit) {
    LazyColumn(
        modifier = Modifier
            .fillMaxSize()
            .background(MaterialTheme.colorScheme.background)
            .padding(horizontal = 20.dp), verticalArrangement = Arrangement.spacedBy(SETTING_SPACING.dp),
        contentPadding = PaddingValues(vertical = 20.dp)
    ) {

        item {
            Preference(
                title = R.string.quests,
                message = stringResource(id = R.string.quest_management),
                icon = R.drawable.folder_open
            ) {
                onPrefPositionClicked(1)
            }
        }

        item {
            Preference(
                title = R.string.preferences_cat_gameplay,
                message = stringResource(id = R.string.gameplay_sum),
                icon = R.drawable.play
            ) {
                onPrefPositionClicked(2)
            }
        }

        item {
            Preference(
                title = R.string.gamepad,
                message = stringResource(id = R.string.gamepad_sum),
                icon = R.drawable.gamepad
            ) {
                onPrefPositionClicked(3)
            }
        }

        item {
            Preference(title = R.string.about, message = stringResource(id = R.string.about_sum), icon = R.drawable.help) {
                onPrefPositionClicked(4)
            }
        }
    }

}