plugins {
    id("com.android.library")
}

android {
    namespace = "org.solarus_games.solarus.deps"
    compileSdk = libs.versions.androidCompileSdk.get().toInt()
    buildToolsVersion = libs.versions.buildTools.get()
    ndkVersion = libs.versions.ndk.get()

    defaultConfig {
        minSdk = libs.versions.androidMinSdk.get().toInt()
    }

    externalNativeBuild {
        cmake {
            version = libs.versions.cmake.get()
            path = File("src/main/cpp/CMakeLists.txt")
        }
    }
}
