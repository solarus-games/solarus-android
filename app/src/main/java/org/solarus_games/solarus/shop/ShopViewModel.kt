package org.solarus_games.solarus.shop

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import org.solarus_games.solarus.common.Resource
import org.solarus_games.solarus.preferences.PreferencesManager
import org.solarus_games.solarus.quest.QuestManager
import org.solarus_games.solarus.remote.models.Game
import org.solarus_games.solarus.remote.use_cases.GetGamesUseCase
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ShopViewModel @Inject constructor(
    private val getGamesUseCase: GetGamesUseCase,
    val preferencesManager: PreferencesManager,
    val questManager: QuestManager
) : ViewModel() {

    var state = MutableStateFlow(ShopState(loading = true))
        private set


    init {
        getGames()
    }

    fun getGames(useCache: Boolean = true) {
        val wrapperFlow = combine(
            getGamesUseCase(useCache),
            preferencesManager.sortDescendingFlow(),
        ) { games, sortDesc -> GameSortWrapper(games, sortDesc) }


        wrapperFlow.onEach { wrapper ->
            with(wrapper) {
                when (wrapper.result) {
                    is Resource.Loading -> state.emit(ShopState(loading = true, games = state.value.games))
                    is Resource.Success -> {
                        val games = result.data ?: state.value.games
                        val result = if (sortDescending) games.sortedByDescending { g -> g.title } else games.sortedBy { g -> g.title }
                        state.emit(ShopState(games = result))
                    }
                    else -> state.emit(ShopState(games = state.value.games, error = result.message.orEmpty()))
                }
            }
        }.launchIn(viewModelScope)
    }
}

data class GameSortWrapper(
    val result: Resource<List<Game>>,
    val sortDescending: Boolean,
)

data class ShopState(
    val loading: Boolean = false,
    val games: List<Game> = emptyList(),
    val error: String = ""
)