package org.solarus_games.solarus.remote

import org.solarus_games.solarus.common.Logger
import org.solarus_games.solarus.quest.QuestManager
import org.solarus_games.solarus.remote.cache.GamesCache
import org.solarus_games.solarus.remote.cache.NewsCache
import org.solarus_games.solarus.remote.models.Article
import org.solarus_games.solarus.remote.models.Game
import javax.inject.Inject

class RemoteRepository @Inject constructor(
    private val logger: Logger,
    private val remoteService: RemoteService,
    private val questManager: QuestManager,
    private val newsCache: NewsCache,
    private val gamesCache: GamesCache,
) {

    suspend fun getGames(useCache: Boolean = true): List<Game> {
        if (useCache) {
            gamesCache.getGames()?.let {
                logger.d(TAG, "Using Games Cache")
                return it.data
            }
        }
        logger.d(TAG, "Fetching Games Data")
        val response = remoteService.getGames()
        gamesCache.setGames(response)
        return response.data
    }

    suspend fun getNews(useCache: Boolean = true): List<Article> {
        if (useCache) {
            newsCache.getNews()?.let {
                logger.d(TAG, "Using News Cache")
                return it.data
            }
        }
        logger.d(TAG, "Fetching News Data")
        val response = remoteService.getNews()
        newsCache.setNews(response)
        return response.data
    }

    suspend fun downloadGame(game: Game): Boolean {
        val result = remoteService.downloadGame(game.download).body()
        return questManager.saveRemoteQuest(result, game.id)
    }

    companion object {
        const val TAG = "RemoteRepository"
    }
}