package org.solarus_games.solarus.common

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.runtime.Composable
import androidx.fragment.app.Fragment
import org.solarus_games.solarus.databinding.HeaderFragmentBinding

// Migrate this class to full compose with Scaffold once performance issues are fixed on JetPack end.
abstract class ComposeFragment : Fragment() {

    @Composable
    abstract fun GetContent()

    @Composable
    abstract fun GetHeader()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding = HeaderFragmentBinding.inflate(inflater)
        with(binding) {
            header.setContent {
                SolarusThemeSurface {
                    GetHeader()
                }
            }
            body.setContent {
                SolarusThemeSurface {
                    GetContent()
                }
            }
            nowPlaying.setContent {
                GetNowPlaying()
            }
        }
        return binding.root
    }

    @Composable
    open fun GetNowPlaying() {
    }
}


