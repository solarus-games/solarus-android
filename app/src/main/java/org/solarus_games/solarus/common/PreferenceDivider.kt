package org.solarus_games.solarus.common

import androidx.annotation.StringRes
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import org.solarus_games.solarus.theme.SecondaryLight

@Composable
fun PreferenceDivider(
    @StringRes title: Int,
) {
    Spacer(modifier = Modifier.height(10.dp))
    Text(stringResource(id = title), color = SecondaryLight, fontSize = 20.sp, fontWeight = FontWeight.Bold)
    Spacer(modifier = Modifier.height(10.dp))
}