package org.solarus_games.solarus.common

import androidx.annotation.StringRes
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material3.Switch
import androidx.compose.material3.SwitchDefaults
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import org.solarus_games.solarus.theme.PrimaryLight
import org.solarus_games.solarus.theme.Secondary
import org.solarus_games.solarus.theme.SecondaryText

@Composable
fun SwitchPreference(
    @StringRes title: Int,
    @StringRes message: Int,
    value: Boolean,
    enabled: Boolean = true,
    onCheckedChange: (value: Boolean) -> Unit
) {
    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier
            .fillMaxWidth()
            .clickable {
                onCheckedChange(!value)
            }
    ) {
        Column(
            modifier = Modifier
                .fillMaxHeight()
                .weight(1f), verticalArrangement = Arrangement.spacedBy(8.dp)
        ) {
            PreferenceHeader(stringResource(id = title), enabled)
            PreferenceMessage(stringResource(id = message), enabled)
        }
        Switch(
            checked = value,
            onCheckedChange = {
                onCheckedChange(it)
            },
            enabled = enabled,
            colors = SwitchDefaults.colors(
                checkedThumbColor = Color.White,
                checkedTrackColor = Secondary,
                uncheckedBorderColor = PrimaryLight,
                uncheckedTrackColor = PrimaryLight,
                uncheckedThumbColor = SecondaryText
            )
        )
    }
}