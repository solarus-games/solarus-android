package org.solarus_games.solarus.remote.use_cases

import kotlinx.coroutines.flow.combineTransform
import org.solarus_games.solarus.remote.models.HomeScreen
import javax.inject.Inject

class GetHomeUseCase @Inject constructor(
    private val getRecentGamesUseCase: GetRecentGamesUseCase,
    private val getNewsUseCase: GetNewsUseCase,
) {

    operator fun invoke(useCache: Boolean = true) =
        combineTransform(getRecentGamesUseCase(), getNewsUseCase(useCache)) { recents, news ->
            emit(HomeScreen(recents = recents, news = news))
        }
}