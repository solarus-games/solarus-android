package org.solarus_games.solarus.preferences

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.Crossfade
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.CenterAlignedTopAppBar
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.MutableStateFlow
import org.solarus_games.solarus.R
import org.solarus_games.solarus.common.ComposeFragment
import org.solarus_games.solarus.theme.Primary
import org.solarus_games.solarus.theme.PrimaryLight
import org.solarus_games.solarus.theme.SecondaryLight
import org.solarus_games.solarus.theme.SecondaryTextLight
import org.solarus_games.solarus.utils.slideInRight
import org.solarus_games.solarus.utils.slideOutRight
import javax.inject.Inject

@AndroidEntryPoint
class PreferencesFragment : ComposeFragment() {

    @Inject
    lateinit var prefsManager: PreferencesManager

    private val indexFlow = MutableStateFlow(0)

    @OptIn(ExperimentalMaterial3Api::class)
    @Composable
    override fun GetHeader() {
        val index by indexFlow.collectAsState()
        Crossfade(targetState = index, label = "Settings Header") {
            when (it) {
                1 -> SettingsHeader(R.string.quests, prefsManager) {
                    indexFlow.value = 0
                }

                2 -> SettingsHeader(R.string.preferences_cat_gameplay, prefsManager) {
                    indexFlow.value = 0
                }

                3 -> SettingsHeader(R.string.gamepad, prefsManager) {
                    indexFlow.value = 0
                }

                4 -> CenterAlignedTopAppBar(
                    colors = TopAppBarDefaults.topAppBarColors(containerColor = Primary),
                    title = {
                        androidx.compose.material.Text(
                            stringResource(id = R.string.about),
                            style = androidx.compose.material.MaterialTheme.typography.h5,
                            modifier = Modifier.padding(vertical = 10.dp),
                            textAlign = TextAlign.Center,
                            color = SecondaryLight
                        )
                    },
                    navigationIcon = {
                        IconButton(onClick = { indexFlow.value = 0 }) {
                            Icon(
                                painter = painterResource(id = R.drawable.arrow_left),
                                contentDescription = "Back",
                                tint = SecondaryTextLight
                            )
                        }
                    }
                )

                else -> CenterAlignedTopAppBar(
                    colors = TopAppBarDefaults.topAppBarColors(containerColor = Primary),
                    title = {
                        Text(
                            stringResource(id = R.string.settings),
                            style = MaterialTheme.typography.headlineLarge,
                            modifier = Modifier.padding(vertical = 10.dp),
                            textAlign = TextAlign.Center,
                            color = SecondaryLight
                        )
                    },
                    actions = {
                        Box {
                            var menuExpanded by remember { mutableStateOf(false) }
                            IconButton(onClick = { menuExpanded = !menuExpanded }) {
                                Icon(
                                    painter = painterResource(id = R.drawable.menu_dots),
                                    contentDescription = "Menu Dots",
                                    tint = SecondaryTextLight
                                )
                            }
                            DropdownMenu(
                                expanded = menuExpanded,
                                onDismissRequest = { menuExpanded = false },
                                modifier = Modifier.background(PrimaryLight)
                            ) {
                                DropdownMenuItem(
                                    text = {
                                        Text(stringResource(R.string.restore_default_settings))
                                    },
                                    onClick = {
                                        menuExpanded = false
                                        prefsManager.resetSettings()
                                    },
                                )
                            }
                        }
                    },
                )
            }
        }
    }

    @Composable
    override fun GetContent() {
        val position by indexFlow.collectAsState()

        Box(Modifier.fillMaxSize()) {
            SettingsView { newPosition -> indexFlow.value = newPosition }

            SettingsItem(position == 1) {
                QuestsView(prefsManager)
            }

            SettingsItem(position == 2) {
                GamePlayView(prefsManager)
            }

            SettingsItem(position == 3) {
                GamepadView(prefsManager)
            }
            SettingsItem(position == 4) {
                AboutView()
            }
        }
    }
}

@Composable
fun SettingsItem(visible: Boolean, content: @Composable () -> Unit) {
    AnimatedVisibility(
        visible = visible,
        enter = slideInRight,
        exit = slideOutRight
    ) {
        content()
    }
}