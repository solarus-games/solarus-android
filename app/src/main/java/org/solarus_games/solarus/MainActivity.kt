package org.solarus_games.solarus

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.widget.Toast
import androidx.annotation.VisibleForTesting
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import dagger.hilt.android.AndroidEntryPoint
import org.solarus_games.solarus.databinding.ActivityMainBinding
import org.solarus_games.solarus.quest.QuestManager
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var questManager: QuestManager

    private lateinit var binding: ActivityMainBinding

    @VisibleForTesting
    var currentFragment: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val hasPermissions = checkPermissions()
        if (!hasPermissions) {
            ActivityCompat.requestPermissions(
                this, PERMISSIONS.toTypedArray(),
                REQUEST_READ_STORAGE
            )
        }

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        navigateToFragment(MainFragment())
        checkIntentForQuest(intent)
    }

    private fun checkPermissions(): Boolean {
        for (permission in PERMISSIONS) {
            if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
                return false
            }
        }
        return true
    }

    private fun navigateToFragment(fragment: Fragment) {
        val fragmentClass = fragment.javaClass.name
        if (fragmentClass != currentFragment) {
            supportFragmentManager.beginTransaction().replace(R.id.main_activity, fragment).commitNowAllowingStateLoss()
            currentFragment = fragment.javaClass.name
        }
    }


    public override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        setIntent(intent)
        checkIntentForQuest(intent)
    }

    private fun checkIntentForQuest(intent: Intent) {
        //Does the activity was launched with a quest to start ?
        intent.getStringExtra(EngineActivity.ARG_PATH)?.let { path ->
            questManager.fromPath(path)?.let { quest ->
                questManager.questLaunchRequest(this, quest)
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            REQUEST_READ_STORAGE -> {
                if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(
                        baseContext,
                        "The app was not allowed to write to your storage. Hence, it cannot function properly. Please consider granting it this permission",
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
        }
    }

    companion object {
        private const val REQUEST_READ_STORAGE = 124

        private val PERMISSIONS = mutableListOf(
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        ).also {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                it.addAll(
                    listOf(
                        Manifest.permission.READ_MEDIA_AUDIO,
                        Manifest.permission.READ_MEDIA_VIDEO,
                        Manifest.permission.READ_MEDIA_IMAGES
                    )
                )
            }
        }
    }
}