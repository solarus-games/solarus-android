package org.solarus_games.solarus.preferences

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import org.solarus_games.solarus.R
import org.solarus_games.solarus.common.Constants.SETTING_SPACING
import org.solarus_games.solarus.common.SwitchPreference

@Composable
fun GamePlayView(
    prefsManager: PreferencesManager,
) {
    LazyColumn(
        modifier = Modifier
            .fillMaxSize()
            .background(MaterialTheme.colorScheme.background)
            .padding(horizontal = 20.dp),
        verticalArrangement = Arrangement.spacedBy(SETTING_SPACING.dp),
        contentPadding = PaddingValues(vertical = 20.dp)
    ) {
        item {
            val value by prefsManager.isAudioEnabledFlow().collectAsState(initial = true)
            SwitchPreference(
                title = R.string.preferences_audio,
                message = R.string.preferences_audio_sum,
                value
            ) {
                prefsManager.setAudioEnabled(it)
            }
        }

        item {
            val value by prefsManager.isForcedLandscapeFlow().collectAsState(initial = true)
            SwitchPreference(
                title = R.string.preferences_force_landscape,
                message = R.string.preferences_force_landscape_sum,
                value
            ) {
                prefsManager.setForcedLandscape(it)
            }
        }
    }

}