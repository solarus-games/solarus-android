import org.jetbrains.kotlin.gradle.plugin.mpp.pm20.util.archivesName

plugins {
    id("com.android.application")
    id("org.jetbrains.kotlin.android")
    id("com.google.devtools.ksp")
    id("dagger.hilt.android.plugin")
    kotlin("plugin.parcelize")
}

android {
    namespace = "org.solarus_games.solarus"
    compileSdk = libs.versions.androidCompileSdk.get().toInt()
    buildToolsVersion = libs.versions.buildTools.get()
    ndkVersion = libs.versions.ndk.get()

    defaultConfig {
        applicationId= "org.solarus_games.solarus"
        minSdk = libs.versions.androidMinSdk.get().toInt()
        targetSdk = libs.versions.androidTargetSdk.get().toInt()
        versionCode = 1
        versionName = "2.0.0"

        vectorDrawables {
            useSupportLibrary = true
        }

        archivesName.set("${rootProject.name}-v${versionName}")
    }

//    signingConfigs {
//        release {
//            storeFile file(signingProps['release.storeFile'] ?: 'release.jks')
//            storePassword signingProps['release.storePassword'] ?: 'solarus'
//            keyAlias signingProps['release.keyAlias'] ?: 'solarus'
//            keyPassword signingProps['release.keyPassword'] ?: 'solarus'
//        }
//    }

    buildTypes {
        release {
//            signingConfig signingConfigs.release
            resValue("string", "app_name", "Solarus Launcher")
            resValue("string", "app_version", "${defaultConfig.versionName}")
        }
        debug {
            applicationIdSuffix = ".debug"
            versionNameSuffix = "-DEBUG"
            resValue("string", "app_name", "Solarus Launcher (Debug)")
            resValue("string", "app_version", "${defaultConfig.versionName}${versionNameSuffix}")
        }
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_17
        targetCompatibility = JavaVersion.VERSION_17
    }

    java {
        sourceCompatibility = JavaVersion.VERSION_17
        targetCompatibility = JavaVersion.VERSION_17
    }

    kotlinOptions {
        jvmTarget = "17"
    }

    buildFeatures {
        buildConfig = true
        viewBinding = true
        compose = true
    }

    composeOptions {
        kotlinCompilerExtensionVersion = libs.versions.composeCompiler.get()
    }

    externalNativeBuild {
        ndkBuild {
            path = File("jni/Android.mk")
        }
    }

    lint {
        disable.add("ExpiredTargetSdkVersion")
    }
}

hilt {
    enableAggregatingTask = false
}

dependencies {
//    implementation(fileTree(include = ['*.jar'], dir: "libs")
    implementation(project(":cmake_build"))
    implementation(libs.androidx.core.ktx)
    implementation(libs.androidx.appcompat)
    implementation(libs.androidx.cardview)
    implementation(libs.androidx.constraintlayout)
    implementation(libs.androidx.legacy.support.v4)
    implementation(libs.androidx.lifecycle.viewmodel.ktx)
    implementation(libs.androidx.preference.ktx)
    implementation(libs.androidx.recyclerview)
    implementation(libs.storage.chooser)
    implementation(libs.material)
    implementation(libs.androidx.window)

    implementation(libs.hilt.android)
    ksp(libs.hilt.android.compiler)
    implementation(libs.androidx.hilt.navigation.compose)
    implementation(libs.androidx.lifecycle.runtime.ktx)
    implementation(libs.androidx.activity.compose)
    implementation(libs.androidx.ui)
    implementation(libs.androidx.ui.tooling.preview)
    implementation(libs.androidx.material)
    implementation(libs.androidx.material3)

    implementation(libs.retrofit)
    implementation(libs.logging.interceptor)
    implementation(libs.converter.moshi)
    implementation(libs.moshi)
    implementation(libs.moshi.kotlin)
    ksp(libs.moshi.kotlin.codegen)

    implementation(libs.coil.compose)

    implementation(libs.androidx.room.runtime)
    implementation(libs.androidx.room.ktx)
    ksp(libs.androidx.room.compiler)
}
