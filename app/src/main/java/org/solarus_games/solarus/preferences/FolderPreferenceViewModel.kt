package org.solarus_games.solarus.preferences

import android.os.Environment
import androidx.annotation.StringRes
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.flow.transform
import org.solarus_games.solarus.R
import java.io.File
import javax.inject.Inject

class FolderPreferenceViewModel @Inject constructor(
    private val preferencesManager: PreferencesManager,
) : ViewModel() {

    private val folderPath = MutableStateFlow("")

    val state = folderPath.transform { path ->
        if (!path.startsWith(Environment.getExternalStorageDirectory().absolutePath)) {
            emit(FolderPrefState(path))
        } else {
            val file = File(path)
            if (!file.exists()) {
                emit(FolderPrefState(path, PathStatus.DOES_NOT_EXIST))
            } else if (file.isDirectory) {
                emit(FolderPrefState(path, PathStatus.VALID))
            } else {
                emit(FolderPrefState(path))
            }
        }
    }.stateIn(viewModelScope, SharingStarted.Eagerly, FolderPrefState())

    init {
        folderPath.value = preferencesManager.getQuestDir().orEmpty()
    }

    fun updatePath(newPath: String) {
        folderPath.value = newPath
    }

    fun setQuestDir() {
        with(state.value) {
            if (status == PathStatus.DOES_NOT_EXIST) {
                val newDir = File(folderPath)
                newDir.mkdir()
            }
            preferencesManager.setQuestDir(folderPath)
        }
    }
}

enum class PathStatus(@StringRes val message: Int) {
    VALID(R.string.valid_path),
    DOES_NOT_EXIST(R.string.path_does_not_exist),
    INVALID(R.string.invalid_path)
}


data class FolderPrefState(val folderPath: String = "", val status: PathStatus = PathStatus.INVALID)